
const INITIAL_STATE = {
    posts: []
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'USER_POSTS_FETCHED':
            return { ...state, posts: action.payload };
            break;
        default:
            return state;
    }
};
