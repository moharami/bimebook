import React, { Component } from 'react';
import  MainDrawerNavigator  from '../navigation';

class App extends Component {

    render() {
        return (
            <MainDrawerNavigator />
        );
    }
}
export default App;
