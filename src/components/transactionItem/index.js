
import React, {Component} from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import asia from '../../assets/asia.png'
import calender from '../../assets/calendar.png'
import money from '../../assets/money.png'

class TransactionItem extends Component {
    constructor(props){
        super(props);
        this.state = {
            modalVisible: false
        };
    }
    setModalVisible() {
        this.setState({modalVisible: !this.state.modalVisible});
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.shaped}>
                    <View style={styles.top}>
                        <View style={styles.centerItem}>
                            <View>
                                <Text style={styles.label}>سررسید</Text>
                                <Text style={styles.value}>97/05/19</Text>
                            </View>
                            <Image source={calender} style={{width: 25, resizeMode: 'contain', tintColor: 'gray', marginLeft: 5}} />
                        </View>
                        <View style={styles.centerItem}>
                            <View>
                                <Text style={styles.label}>مبلغ</Text>
                                <Text style={styles.value}>1,250,000 تومان</Text>
                            </View>
                            <Image source={money} style={{width: 25, resizeMode: 'contain', tintColor: 'gray', marginLeft: 5}} />
                        </View>
                    </View>
                    <View style={styles.bottom}>
                        <Text style={styles.value}>بابت حق بیمه زلزله پارسیان</Text>
                        <Text style={[styles.label, {fontSize: 14}]}>شرح: </Text>
                    </View>
                </View>
            </View>
        );
    }
}
export default TransactionItem;