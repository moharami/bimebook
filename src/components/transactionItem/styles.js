import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        marginBottom: 20
    },
    shaped: {
        borderRadius: 10,
        marginLeft: 17
    },
    top: {
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        backgroundColor: 'white',
        padding: 7,
        paddingBottom: 2,
        paddingRight: 14,
        paddingLeft: 14,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        borderBottomColor: 'rgb(237, 237, 237)',
        borderBottomWidth: 1,
    },
    label: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'gray'
    },
    value: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 13,
        color: 'black'
    },
    bottom: {
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        paddingTop: 8,
        paddingBottom: 8,
        paddingRight: 14,
        paddingLeft: 14,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        backgroundColor: 'white'
    },
    topTxt: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'white',
    },
    bottomTxt: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'rgb(142, 199, 61)',
    },
    footerLabel: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        paddingLeft: 5
    },
    center: {
        flexDirection: 'row',
        width: '100%',
    },
    centerItem: {
        width: '50%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
});
