import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    ccontainer: {
        // flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        backgroundColor: 'rgb(247, 247, 247)',
        width: '37%',
        borderRadius: 15,
        height: 50,
        paddingRight: 20,
        marginRight: 15,
        marginBottom: 15
    },
    cimage: {
        width: '27%',
        resizeMode: 'contain',
        // marginRight: '30%'
    },
    ctitle: {
        color: 'black',
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingRight: '13%'
    },

});
