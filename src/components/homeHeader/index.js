import React, {Component} from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/EvilIcons';
import {Actions} from 'react-native-router-flux'
import headerImg from '../../assets/bimebook-logo-home.png'
class HomeHeader extends Component {

    constructor(props){
        super(props);
        this.state = {
        };
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.top}>
                    <Image source={headerImg} style={[styles.headerImage, {marginRight: this.props.notification === false ? '40%' : '27%'}]} />
                    {
                        this.props.notification === false ? null :
                            <TouchableOpacity onPress={() =>this.props.openDrawer()} style={styles.notifContainer}>
                                <Icon name="envelope" size={33} color="white" />
                                <View style={styles.logoContainer}>
                                    <Text style={styles.logoText}>2</Text>
                                </View>
                            </TouchableOpacity>
                    }

                </View>
            </View>
        );
    }
}
export default HomeHeader;