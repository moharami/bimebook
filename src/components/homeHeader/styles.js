import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        // flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent: 'space-between',
        // backgroundColor: 'white',
        height: 120,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
        paddingRight: 15,
        paddingLeft: 15,
        // paddingTop: 50,
        // paddingBottom: 100,

    },
    headerImage: {
        width: '22%',
        resizeMode: 'contain',
        marginRight: '30%'
    },
    top: {
        flexDirection: 'row',
        alignItems: "flex-start",
        justifyContent: 'flex-end',
        paddingTop: 13
        // paddingTop: 30,
        // paddingBottom: 30
    },
    headerTitle: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingRight: '32%'
    },
    name: {
        color: 'gray',
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    notifContainer: {
        position: 'relative',
        zIndex: 1,
        padding: 5
    },
    logoContainer: {
        height: 15,
        width: 15,
        borderRadius: 15,
        backgroundColor: "red",
        justifyContent: "center",
        alignItems: "center",
        position: 'absolute',
        top: 2,
        right: 2,
        zIndex: 20


    },
    logoText: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 18,
        color: "white"
    },
});