import React, {Component} from 'react';
import {View} from 'react-native';
import {List, SearchBar} from "react-native-elements";

class AppSearchBar extends Component {
    constructor(props){
        super(props);
        this.state = {
            value: ''
        };
    }
    _doSearch(text){
    }
    render() {
        return (
            <View style={{width: '100%'}}>
                <SearchBar
                    containerStyle={{
                        flex: 1,
                        backgroundColor: 'white',
                        width: '100%',
                        borderWidth: 0,
                        shadowColor: 'white',
                        borderBottomColor: 'transparent',
                        borderTopColor: 'transparent',
                        marginBottom: 20

                    }}
                    onChangeText={(text) => this._doSearch(text)}
                    onClearText={() => this.setState({value: ''})}
                    placeholder="جتجوی شرکت بیمه..."
                    inputStyle={{textAlign: 'right',  paddingRight: '5%', backgroundColor: 'white', borderColor: 'rgb(50, 50, 50)', borderWidth: 1, borderRadius: 18}}
                />
            </View>
        );
    }
}
export default AppSearchBar;