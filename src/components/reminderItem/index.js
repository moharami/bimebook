
import React, {Component} from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import asia from '../../assets/asia.png'
class ReminderItem extends Component {
    constructor(props){
        super(props);
        this.state = {
        };
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.reminder}>
                    <Icon name="bell-o" size={25} color="white" />
                </View>
                <View>
                    <Text style={styles.label}>نوع بیمه</Text>
                    <Text style={styles.value}>شخص ثالث</Text>
                </View>
                <View>
                    <Text style={styles.label}>سررسید</Text>
                    <Text style={styles.value}>97/05/19</Text>
                </View>
                <Image source={asia} style={{width: 50, height: 50, resizeMode: 'contain'}} />
            </View>
        );
    }
}
export default ReminderItem;