import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 15
    },
    reminder: {
        zIndex: 40,
        backgroundColor: '#fdb913',
        borderRadius: 50,
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    rightContainer: {
        // flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 3,
        paddingLeft: 3,
        height: 27,
        // borderRadius: 20,
        backgroundColor: 'white',
        // overflow: 'hidden'
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderTopLeftRadius: 10,
        marginRight: 5
        // overflow: 'hidden',

    },
    labelContainer: {
        flexDirection: 'row',
        paddingRight: 10

    },
    label: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'gray'
    },
    value: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 13,
        color: 'black'
    },
    title: {
        fontFamily: 'IRANSansMobile(FaNum)_Bold',
        fontSize: 18,
        color: 'white'
    },
    contentText: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'white',
        paddingRight: 10
    },



});