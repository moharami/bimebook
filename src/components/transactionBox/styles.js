import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: .5,
        width: '49%',
        // padding: 10,
        // backgroundColor: 'white',
        // borderRadius: 10,
        // flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent: 'space-between',
        marginBottom: 30
    },
    shaped: {
        borderRadius: 10,
        // borderColor: 'rgb(142, 199, 61)',
        // borderWidth: 1,
        // width: '80%'
        marginLeft: 17
    },
    top: {
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        backgroundColor: 'white',
        padding: 10,
        paddingBottom: 2,
        paddingRight: 14,
        paddingLeft: 14,
        alignItems: 'center',
        justifyContent: 'center'
    },
    label: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'gray'
    },
    value: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14,
        color: 'black'
    },
    bottom: {
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        paddingTop: 2,
        paddingBottom: 2,
        paddingRight: 14,
        paddingLeft: 14,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgb(11, 81, 144)'
    },
    topTxt: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'white',
    },
    bottomTxt: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'rgb(142, 199, 61)',
    },
    footerLabel: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        paddingLeft: 5
    },
});