
import React, {Component} from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import asia from '../../assets/asia.png'
import DetailModal from './detailModal'

class TransactionBox extends Component {
    constructor(props){
        super(props);
        this.state = {
            modalVisible: false
        };
    }
    setModalVisible() {
        this.setState({modalVisible: !this.state.modalVisible});
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.shaped}>
                    <View style={styles.top}>
                        <Text style={styles.label}>{this.props.title}</Text>
                        <Text style={styles.value}>{this.props.value}</Text>
                    </View>
                    <TouchableOpacity onPress={() => this.setModalVisible()} style={styles.bottom}>
                        <Icon name="angle-down" size={16} color="white" />
                        <Text style={styles.footerLabel}>جزئیات</Text>
                        {/*<EIcon name="close-o" size={18} color="red" />*/}
                    </TouchableOpacity>
                </View>
                <DetailModal
                    closeModal={() => this.closeModal()}
                    modalVisible={this.state.modalVisible}
                    onChange={(visible) => this.setModalVisible(visible)}
                />
            </View>
        );
    }
}
export default TransactionBox;