import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    topContainer: {
        flex: 1,
        width: '100%',
        marginBottom: 15,
        backgroundColor: 'white',
        borderRadius: 10

    },
    container: {
        // flex: 1,
        width: '100%',
        padding: 10,
        borderRadius: 10,
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    reminder: {
        zIndex: 40,
        backgroundColor: '#fdb913',
        borderRadius: 50,
        padding: 7,
        alignItems: 'center',
        justifyContent: 'center'
    },
    rightContainer: {
        // flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 3,
        paddingLeft: 3,
        height: 27,
        // borderRadius: 20,
        backgroundColor: 'white',
        // overflow: 'hidden'
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderTopLeftRadius: 10,
        marginRight: 5
        // overflow: 'hidden',

    },
    labelContainer: {
        flexDirection: 'row',
        paddingRight: 10

    },
    label: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'gray'
    },
    value: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 13,
        color: 'black'
    },
    title: {
        fontFamily: 'IRANSansMobile(FaNum)_Bold',
        fontSize: 18,
        color: 'white'
    },
    contentText: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'white',
        paddingRight: 10
    },
    rightRow: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
    },
    leftRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    shaped: {
        borderRadius: 10,
        borderColor: 'rgb(142, 199, 61)',
        borderWidth: 1,
        // width: '80%'
        marginLeft: 17
    },
    top: {
       borderTopRightRadius: 10,
       borderTopLeftRadius: 10,
        backgroundColor: 'rgb(11, 81, 144)',
        paddingTop: 2,
        paddingBottom: 2,
        paddingRight: 14,
        paddingLeft: 14,
        alignItems: 'center',
        justifyContent: 'center'
    },
    bottom: {
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        paddingTop: 2,
        paddingBottom: 2,
        paddingRight: 14,
        paddingLeft: 14,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    topTxt: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'white',
    },
    bottomTxt: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'rgb(142, 199, 61)',
    },
    center: {
        flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent: 'center',
        width: '50%',
    },
    centerItem: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderTopColor: 'rgb(237, 237, 237)',
        borderTopWidth: 1,
        borderBottomColor: 'rgb(237, 237, 237)',
        borderBottomWidth: 1
    },
    advertise: {
        width: '30%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#8dc63f',
        padding: 5,
        // marginTop: 20

    },
    buttonTitle: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    footer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingRight: 15,
        paddingLeft: 15,
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: 'rgb(252, 252, 252)',
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,

    },
    footerLabel: {
        color: 'rgb(11, 81, 144)',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        paddingLeft: 5
    },
    detail: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    }



});
