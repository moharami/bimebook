
import React, {Component} from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import EIcon from 'react-native-vector-icons/dist/EvilIcons';
import asia from '../../assets/asia.png'
import calender from '../../assets/calendar.png'
import insurancePaper from '../../assets/insurance-paper.png'
import Actions from 'react-native-router-flux'

class UserInsuranceItem extends Component {
    constructor(props){
        super(props);
        this.state = {
        };
    }
    render() {
        return (
        <View style={styles.topContainer}>
            <View style={styles.container}>
                <View style={styles.leftRow}>
                    <View style={styles.reminder}>
                        <Icon name="bell-o" size={16} color="white" />
                    </View>
                    <View style={styles.shaped}>
                        <View style={styles.top}>
                            <Text style={styles.topTxt}>بیمه آسیا</Text>
                        </View>
                        <View style={styles.bottom}>
                            <Text style={styles.bottomTxt}>صادرشده</Text>
                            <EIcon name="check" size={18} color="rgb(142, 199, 61)" />
                            {/*<EIcon name="close-o" size={18} color="red" />*/}
                        </View>
                    </View>
                </View>
                <View style={styles.rightRow}>
                    <Text style={styles.value}>بیمه آسیا</Text>
                    <Image source={asia} style={{width: 50, height: 50, resizeMode: 'contain'}} />
                </View>
            </View>
            <View style={styles.center}>
                <View style={styles.centerItem}>
                    <View>
                        <Text style={styles.label}>سررسید بیمه نامه</Text>
                        <Text style={styles.value}>97/05/19</Text>
                    </View>
                    <Image source={calender} style={{width: 25, resizeMode: 'contain', tintColor: 'gray', marginLeft: 5}} />
                </View>
                <View style={styles.centerItem}>
                    <View>
                        <Text style={styles.label}>شماره بیمه نامه</Text>
                        <Text style={styles.value}>78784548787845</Text>
                    </View>
                    <Image source={insurancePaper} style={{width: 25, resizeMode: 'contain', tintColor: 'gray', marginLeft: 5}} />
                </View>
            </View>
            <View style={styles.footer}>
                <TouchableOpacity onPress={() => null} style={styles.advertise}>
                    <Text style={styles.buttonTitle}>تمدید</Text>
                </TouchableOpacity>
                <View style={styles.detail}>
                    <Icon name="angle-down" size={16} color="rgb(11, 81, 144)" />
                    <Text style={styles.footerLabel}>جزئیات بیمه نامه</Text>
                </View>

            </View>
        </View>
        );
    }
}
export default UserInsuranceItem;