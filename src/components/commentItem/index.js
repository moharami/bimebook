
import React, {Component} from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import SIcon from 'react-native-vector-icons/dist/SimpleLineIcons';
import {Actions} from 'react-native-router-flux'
import user from '../../assets/user.png'

class CommentItem extends Component {
    constructor(props){
        super(props);
        this.state = {
        };
    }
    render() {
        return (
            <View style={styles.commentContainer}>
                <View style={styles.contentContainer}>
                    <View style={styles.headerContainer}>
                        <Text style={styles.title}>بیمه آسیا</Text>
                        <View style={styles.labelContainer}>
                            <Text style={styles.label}>4.2</Text>
                        </View>
                    </View>
                    <Text style={styles.content}>خسیخشهیخحسشبحححح   حخبهخحسهیبحس سیب حخهب  یسخبحسیب ححخبح سب بخهبی سب dبیمه آسیا</Text>
                </View>
                <View style={styles.imageContainer}>
                    <Image source={user} style={styles.image} />

                </View>
            </View>
        );
    }
}
export default CommentItem;