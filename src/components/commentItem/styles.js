import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    commentContainer: {
        flex: 1,
        width: '100%',
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
        flexDirection: 'row',
        padding: 5,
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: 'rgb(207, 207, 207)'
    },
    contentContainer: {
        // paddingRight: 15
        width: '80%'
    },
    headerContainer: {
        alignItems: 'center',
        justifyContent: 'flex-end',
        flexDirection: 'row'
    },
    title: {
        paddingRight: 10,
        fontSize: 15,
        color: 'rgb(50, 50, 50)'
    },
    labelContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 3,
        paddingLeft: 3,
        backgroundColor: 'rgb(253, 183, 11)',
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderTopLeftRadius: 10,
        marginRight: 5
    },
    label: {
        paddingRight: 4,
        paddingLeft: 4,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12
    },
    content: {
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingLeft: 5,
        lineHeight: 25
    },
    imageContainer: {
        borderRadius: 100,
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgb(168, 170, 173)',
        marginRight: 10,
        marginLeft: 10
    },
    image: {
        width: 20,
        height: 20,
        tintColor: 'white'
    }
});