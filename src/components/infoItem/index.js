
import React, {Component} from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/EvilIcons';
import {Actions} from 'react-native-router-flux'
import headerImg from '../../assets/bimebook-logo-home.png'

class InfoItem extends Component {
    constructor(props){
        super(props);
        this.state = {
        };
    }
    render() {
        return (
            <View style={[styles.container, {width: this.props.long ? '100%': '49%', backgroundColor: this.props.yellow ? '#fdb913' : 'rgb(247, 247, 247)' }]}>
                <Text style={[styles.label, {color: this.props.yellow ? 'white': undefined }]}>{this.props.label}</Text>
                <Text style={styles.value}> امیر حسین اکبری</Text>
            </View>
        );
    }
}
export default InfoItem;