import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        width: '49%',
        padding: 10,
        backgroundColor: 'rgb(247, 247, 247)',
        borderRadius: 10,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    label: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 11,
        color: "gray"
    },
    value: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 13,
        color: "black"
    }
});