import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        width: '95%',
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 15,
        marginBottom: 15
    },
    rowContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    statusContainer: {
        paddingRight: 7,
        paddingLeft: 7,
        // paddingTop: 3,
        // paddingBottom: 3,
        backgroundColor: 'rgb(142, 199, 61)',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 15,



        borderTopLeftRadius: 20,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        // borderTopRightRadius: 20,
    },
    label: {
        fontSize: 12,
        color: 'gray',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    value: {
        fontSize: 13,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    statusTxt: {
        fontSize: 12,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)'
    }
});
