
import React, {Component} from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {Actions} from 'react-native-router-flux'
import headerImg from '../../assets/bimebook-logo-home.png'
import asia from '../../assets/asia.png'
import SIcon from 'react-native-vector-icons/dist/SimpleLineIcons';

class CourierItem extends Component {
    constructor(props){
        super(props);
        this.state = {

        };
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.rowContainer}>
                    <SIcon name="arrow-left" size={14} color="gray" />
                    <View style={styles.statusContainer}>
                        <Text style={styles.statusTxt}>در انتظار پیک</Text>
                    </View>
                    <View>
                        <Text style={styles.label}>نوع بیمه</Text>
                        <Text style={styles.value}>شخص ثالث</Text>
                    </View>
                    <View>
                        <Text style={styles.label}>بیمه نامه</Text>
                        <Text style={styles.value}>1289875465</Text>
                    </View>
                </View>
            </View>
        );
    }
}
export default CourierItem;