


import React, {Component} from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/EvilIcons';
import {Actions} from 'react-native-router-flux'
import headerImg from '../../assets/bimebook-logo-home.png'

class Address extends Component {
    constructor(props){
        super(props);
        this.state = {
        };
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.rightContainer}>
                    <Text style={styles.label}>منزل</Text>
                </View>
                <Text style={styles.value}> تهران، خیابان میرداماد، نفت شمالی،، تفت شمالی، پلاکک 50</Text>

            </View>
        );
    }
}
export default Address;