import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        width: 250,
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 10,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        marginLeft: 10,
        paddingBottom: 30
    },
    label: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 11,
        color: "white"
    },
    value: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 13,
        color: "black",
        lineHeight: 25
    },
    rightContainer: {
        // flex: 1,

        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 7,
        paddingLeft: 7,
        height: 27,
        backgroundColor: 'rgb(168, 170, 173)',
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderTopLeftRadius: 10,
        marginRight: 5

    },
});