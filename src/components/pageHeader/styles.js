import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        paddingRight: 15,
        paddingLeft: 15,
        // paddingTop: 50,
        // paddingBottom: 100,
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
    },
    top: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        // paddingTop: 20
        // paddingTop: 30,
        // paddingBottom: 30
    },
    headerTitle: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingRight: '32%'
    },
    name: {
        color: 'gray',
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    logoText: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 16,
        color: "white",
    },
});