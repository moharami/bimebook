import React, {Component} from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/SimpleLineIcons';
import setting from '../../assets/setting.png';
import {Actions} from 'react-native-router-flux'
import headerImg from '../../assets/bimebook-logo-home.png'

class PageHeader extends Component {
    constructor(props){
        super(props);
        this.state = {
        };
    }
    render() {
        return (
            <View style={styles.container}>
                    <Icon name="arrow-left" size={20} color="white" />
                    <Text style={styles.logoText}>{this.props.title}</Text>
                {
                    this.props.carInfo ?
                    <TouchableOpacity onPress={()=> null} style={styles.notifContainer}>
                        <Image source={setting} style={{tintColor: 'white', width: 25, resizeMode: 'contain'}} />
                    </TouchableOpacity> : <Text></Text>
                }

            </View>
        );
    }
}
export default PageHeader;