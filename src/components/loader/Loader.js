import React from 'react';
import {View, Text, ActivityIndicator} from 'react-native';

import styles from './styles';

const Loader = (props) => {
    return (
        <View style={styles.container}>
            <ActivityIndicator
                size={props.size}
                animating={props.animating}
                color={props.color}
                {...props}
            />
            <Text style={[styles.text, {color: '#353b48', fontFamily: 'IRANSansMobile(FaNum)'}]}>{props.send ? 'در حال دریافت اطلاعات' : 'در حال دریافت اطلاعات'}</Text>
        </View>
    );
};
Loader.defaultProps = {
    size: 'large',
    animating: true,
    color: '#353b48',
    send:true
};
export default Loader;

