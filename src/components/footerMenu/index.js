import React from 'react';
import {Text, View, TouchableOpacity, AsyncStorage, Alert, Image} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import SIcon from 'react-native-vector-icons/dist/SimpleLineIcons';
import FIcon from 'react-native-vector-icons/dist/Feather';
import MIcon from 'react-native-vector-icons/dist/MaterialIcons';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Axios from 'axios';
import archive_paper from '../../assets/archive-paper.png'
import money from '../../assets/money.png'
import building from '../../assets/building.png'
export const url = 'http://bani.azarinpro.info/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'

export default class FooterMenu extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            active: 0,
            loading: false,
            more: false
        };
    }
    render(){
        // if(this.state.loading){
        //     return (<Loader />)
        // }
        // else
        return (
            <View>
                {
                    this.state.more || this.props.showMore ?
                        <View style={styles.container}>
                            <TouchableOpacity onPress={() => null}>
                                <View style={styles.navContainer}>
                                    <SIcon name="home" size={24} color={this.props.active === 'home' ? 'rgb(20, 85, 151)': 'gray'} />
                                    <Text style={{color:this.props.active === 'home' ? 'rgb(20, 85, 151)': 'gray', fontSize: 12, fontFamily: 'IRANSansMobile(FaNum)'}} >خانه</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => null}>
                                <View style={styles.navContainer}>
                                    <FIcon name="user" size={24} color={this.props.active === 'profile' ? 'rgb(20, 85, 151)': 'gray'} />
                                    <Text style={{color: this.props.active === 'profile' ? 'rgb(20, 85, 151)': 'gray', fontSize: 12, fontFamily: 'IRANSansMobile(FaNum)'}} >پروفایل</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => null}>
                                <View style={styles.navContainer}>
                                    <Image source={archive_paper} style={{tintColor: this.props.active === 'bime' ? 'rgb(20, 85, 151)': 'gray', width: 23, height: 23}} />
                                    <Text style={{color:this.props.active === 'bime' ? 'rgb(20, 85, 151)': 'gray', fontSize: 12, fontFamily: 'IRANSansMobile(FaNum)'}} >بیمه های من</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => null}>
                                <View style={styles.navContainer}>
                                    <Image source={building} style={{tintColor: this.props.active === 'company' ? 'rgb(20, 85, 151)': 'gray', width: 23, height: 23}} />
                                    <Text style={{color:this.props.active === 'company' ? 'rgb(20, 85, 151)': 'gray', fontSize: 12, fontFamily: 'IRANSansMobile(FaNum)'}} >شرکت ها</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => null}>
                                <View style={styles.navContainer}>
                                    <Image source={money} style={{tintColor: this.props.active === 'financial' ? 'rgb(20, 85, 151)': 'gray', width: 23, height: 23}} />
                                    <Text style={{color: this.props.active === 'financial' ? 'rgb(20, 85, 151)': 'gray', fontSize: 12, fontFamily: 'IRANSansMobile(FaNum)'}}>مالی</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => null}>
                                <View style={styles.navContainer}>
                                    <Image source={archive_paper} style={{tintColor: this.props.active === 'bime' ? 'rgb(20, 85, 151)': 'gray', width: 23, height: 23}} />
                                    <Text style={{color: this.props.active === 'more' ? 'rgb(20, 85, 151)': 'gray', fontSize: 12, fontFamily: 'IRANSansMobile(FaNum)'}}>سفارشات</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => null}>
                                <View style={styles.navContainer}>
                                    <MIcon name="motorcycle" size={24} color={this.props.active === 'more' ? 'rgb(20, 85, 151)': 'gray'} />
                                    <Text style={{color: this.props.active === 'more' ? 'rgb(20, 85, 151)': 'gray', fontSize: 12, fontFamily: 'IRANSansMobile(FaNum)'}}>پیک</Text>
                                </View>
                            </TouchableOpacity>
                        </View> :
                        <View style={styles.container}>
                            <TouchableOpacity onPress={() => null}>
                                <View style={styles.navContainer}>
                                    <SIcon name="home" size={24} color={this.props.active === 'home' ? 'rgb(20, 85, 151)': 'gray'} />
                                    <Text style={{color:this.props.active === 'home' ? 'rgb(20, 85, 151)': 'gray', fontSize: 12, fontFamily: 'IRANSansMobile(FaNum)'}} >خانه</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => null}>
                                <View style={styles.navContainer}>
                                    <FIcon name="user" size={24} color={this.props.active === 'profile' ? 'rgb(20, 85, 151)': 'gray'} />
                                    <Text style={{color: this.props.active === 'profile' ? 'rgb(20, 85, 151)': 'gray', fontSize: 12, fontFamily: 'IRANSansMobile(FaNum)'}} >پروفایل</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => null}>
                                <View style={styles.navContainer}>
                                    <Image source={archive_paper} style={{tintColor: this.props.active === 'bime' ? 'rgb(20, 85, 151)': 'gray', width: 23, height: 23}} />
                                    <Text style={{color:this.props.active === 'bime' ? 'rgb(20, 85, 151)': 'gray', fontSize: 12, fontFamily: 'IRANSansMobile(FaNum)'}} >بیمه های من</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => null}>
                                <View style={styles.navContainer}>
                                    <Image source={building} style={{tintColor: this.props.active === 'company' ? 'rgb(20, 85, 151)': 'gray', width: 23, height: 23}} />
                                    <Text style={{color:this.props.active === 'company' ? 'rgb(20, 85, 151)': 'gray', fontSize: 12, fontFamily: 'IRANSansMobile(FaNum)'}} >شرکت ها</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.setState({more: !this.state.more})}>
                                <View style={styles.navContainer}>
                                    <FIcon name="more-horizontal" size={24} color={this.props.active === 'more' ? 'rgb(20, 85, 151)': 'gray'} />
                                    <Text style={{color: this.props.active === 'more' ? 'rgb(20, 85, 151)': 'gray', fontSize: 12, fontFamily: 'IRANSansMobile(FaNum)'}}>بیشتر</Text>
                                </View>
                            </TouchableOpacity>
                        </View>





                }
            </View>
        );
    }
}