
import React, {Component} from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/Feather';
import MIcon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import setting from '../../assets/setting.png';
import {Actions} from 'react-native-router-flux'
import headerImg from '../../assets/bimebook-logo-home.png'
import asia from '../../assets/asia.png'

class ReminderInput extends Component {
    constructor(props){
        super(props);
        this.state = {
        };
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.section}>
                    <Icon name="edit" size={20} color="black" style={{paddingRight: 10}} />
                    <Text style={styles.value}>یک هفته قبل</Text>
                </View>
                <View style={styles.section}>
                    <Text style={styles.value}>091236528993</Text>
                    <MIcon name="message-text-outline" size={20} color="gray" style={{paddingLeft: 10}} />

                </View>
            </View>
        );
    }
}
    export default ReminderInput;