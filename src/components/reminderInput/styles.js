
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        width: '93%',
        padding: 13,
        backgroundColor: 'white',
        borderRadius: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 15,
        borderWidth: 1,
        borderColor: 'rgb(207, 207, 207)'
    },
    value: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'rgb(60, 60, 60)'
    },
    title: {
        fontFamily: 'IRANSansMobile(FaNum)_Bold',
        fontSize: 18,
        color: 'white'
    },
    section: {
        flexDirection: 'row'
    }
});
