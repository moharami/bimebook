import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    topContainer: {
        width: '90%',
        // height: '100%',
        borderRadius: 10,
        marginBottom: 15,
        // alignItems: 'center',
        // justifyContent: 'center',
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
        borderRadius: 10
    },
    header: {
        borderRadius: 10,
        paddingTop: 10,
        paddingBottom: 10,
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
        // borderBottomColor: 'rgb(237, 237, 237)',
        // borderBottomWidth: 1,
        paddingLeft: 10,
        // paddingBottom: 10
    },
    info: {
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        padding: 10
    },
    price: {
        flexDirection: 'row'
    },
    priceLabel: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        paddingRight: 5
    },
    amount: {
        fontSize: 16,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    Image: {
        width: 50,
        resizeMode: 'contain',
        height: 50,
        marginTop: 5,
        marginRight: 10
    },
    rightContainer: {
        // flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 3,
        paddingLeft: 3,
        // borderRadius: 20,
        backgroundColor: 'rgba(122, 130, 153, 1)',
        // overflow: 'hidden'
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderTopRightRadius: 10,
        // overflow: 'hidden',

    },
    label: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'white',
        // paddingLeft: 10,
        // paddingRight: 10
    },
    value: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'white',
        fontSize: 10
    },
    redValue: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'red',
        fontSize: 12
    },
    regimContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 8,
    },
    body: {
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomRightRadius: 15,
        borderBottomLeftRadius: 15,
        backgroundColor: 'rgb(20, 85, 151)'
    },
    bodyLeft: {
        flexDirection: 'row',
        paddingLeft: 30
    },
    bodyRight: {
        flexDirection: 'row'
    },
    bodyLabel: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 10
    },
    bodyValue: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 10
    },
    iconLeftContainer: {
        backgroundColor: 'rgba(255, 45, 85, 1)',
        width: 32,
        height: 32,
        borderRadius: 32,
        alignItems: 'center',
        justifyContent: 'center',
    },
    iconRightContainer: {
        // height: 32,
        // paddingLeft: 3,
        // borderRadius: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    footer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 5,
        backgroundColor: 'rgba(245, 246, 250, 1)',
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderTopColor: 'rgb(237, 237, 237)',
        borderTopWidth: 1,

    },
    startContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    iconRightEditContainer: {
        backgroundColor: 'rgba(51, 197, 117, 1)',
        width: 32,
        height: 32,
        borderRadius: 32,
        alignItems: 'center',
        justifyContent: 'center'
    },
    advertise: {
        width: '30%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#8dc63f',
        padding: 8,
        marginTop: 20

    },
    buttonTitle: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    top: {
        flexDirection: 'row',
        width: '100%',
        borderBottomColor: 'rgb(157, 157, 157)',
        borderBottomWidth: 1,
        paddingBottom: 20
    },
    left: {
        width: '50%',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    // leftImage: {
    //     alignItems: 'center',
    //     justifyContent: 'space-between'
    // },
    right: {
        width: '50%',
        // alignItems: 'center',
        // justifyContent: 'space-between'
    },
    item: {
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    middle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomColor: 'rgb(157, 157, 157)',
        borderBottomWidth: 1,
        // paddingTop: 15,
        // paddingBottom: 15
        padding: 10
    },
    middleItem: {
        flexDirection: 'row'
    },
    bottom: {
        alignItems: 'center',
        justifyContent: 'center'
    }
});
