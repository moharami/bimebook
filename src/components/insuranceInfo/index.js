import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import OIcon from 'react-native-vector-icons/dist/Octicons';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import pic2 from '../../assets/insurances/2.png'
import pic3 from '../../assets/insurances/3.png'
import pic4 from '../../assets/insurances/4.png'
import pic5 from '../../assets/insurances/5.png'
import pic6 from '../../assets/insurances/6.png'
import pic7 from '../../assets/insurances/7.png'
import pic8 from '../../assets/insurances/8.png'
import pic9 from '../../assets/insurances/9.png'
import pic10 from '../../assets/insurances/10.png'
import pic11 from '../../assets/insurances/11.png'
import pic12 from '../../assets/insurances/12.png'
import pic13 from '../../assets/insurances/13.png'
import pic14 from '../../assets/insurances/14.png'
import pic15 from '../../assets/insurances/15.png'
import pic18 from '../../assets/insurances/18.png'
import pic19 from '../../assets/insurances/19.png'
import pic20 from '../../assets/insurances/20.png'
import pic21 from '../../assets/insurances/21.png'
import pic22 from '../../assets/insurances/22.png'
import pic23 from '../../assets/insurances/23.png'

class InsuranceInfo extends Component {
    constructor(props){
        super(props);
        this.state = {
            open: false,
            detail: false
        };
    }
    render() {
        const id = 5;
        let imgSrc = null;
        switch (id) {
            case 2:
                imgSrc = pic2;
                break;
            case 3:
                imgSrc = pic3;
                break;
            case 4:
                imgSrc = pic4;
                break;
            case 5:
                imgSrc = pic5;
                break;
            case 6:
                imgSrc = pic6;
                break;
            case 7:
                imgSrc = pic7;
                break;
            case 8:
                imgSrc = pic8;
                break;
            case 9:
                imgSrc = pic9;
                break;
            case 10:
                imgSrc = pic10;
                break;
            case 11:
                imgSrc = pic11;
                break;
            case 12:
                imgSrc = pic12;
                break;
            case 13:
                imgSrc = pic13;
                break;
            case 14:
                imgSrc = pic14;
                break;
            case 15:
                imgSrc = pic5;
                break;
            case 18:
                imgSrc = pic18;
                break;
            case 19:
                imgSrc = pic19;
                break;
            case 20:
                imgSrc = pic20;
                break;
            case 21:
                imgSrc = pic21;
                break;
            case 22:
                imgSrc = pic22;
                break;
            case 23:
                imgSrc = pic23;
                break;
        }
        return (
            <View style={styles.topContainer}  onPress={() => null}>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <TouchableOpacity onPress={() => this.setState({detail: !this.state.detail})} style={styles.iconRightContainer}>
                            {
                                this.state.detail ? <FIcon name="chevron-up" size={18} color="gray" /> : <FIcon name="chevron-down" size={18} color="gray" />


                            }
                            <Text style={[styles.label, {color: 'black', fontFamily: 'IRANSansMobile(FaNum)_Bold', fontSize: 16}]}>122220000 تومان </Text>
                        </TouchableOpacity>
                        <View style={styles.leftImage}>
                            <Image source={imgSrc} style={styles.Image} />
                        </View>
                    </View>
                    {
                        this.state.detail?
                            <View style={styles.body}>
                                <View style={styles.top}>
                                    <View style={styles.left}>
                                        <View style={styles.item}>
                                            <Text style={styles.value}>8</Text>
                                            <Text style={styles.label}>تعداد شعب پرداخت خسارت</Text>
                                        </View>
                                        <View style={styles.item}>
                                            <Text style={styles.value}>5%</Text>
                                            <Text style={styles.label}>سهم از بازار</Text>
                                        </View>
                                        <View style={styles.item}>
                                            <Text style={styles.value}>ندارد</Text>
                                            <Text style={styles.label}>پرداخت خسارت سیار</Text>
                                        </View>
                                    </View>
                                    <View style={styles.right}>
                                        <View style={styles.item}>
                                            <Text style={styles.value}>100</Text>
                                            <Text style={styles.label}>رضایت مشتری</Text>
                                        </View>
                                        <View style={styles.item}>
                                            <Text style={styles.value}>2</Text>
                                            <Text style={styles.label}>زمان پاسخگویی به شکایات(روز)</Text>
                                        </View>
                                        <View style={styles.item}>
                                            <View style={styles.startContainer}>
                                                {
                                                    [1, 2, 3].map((item, index)=> <OIcon key={index} name="dash" size={30} color="#fdb913" style={{paddingRight: 3, borderRadius: 10}} />
                                                    )
                                                }
                                                {/*<Text style={styles.value}>سطوح توانگری</Text>*/}

                                            </View>
                                            <Text style={styles.label}>سطوح توانگری</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.middle}>
                                    <View style={styles.middleItem}>
                                        <Text style={styles.label}>6666666 تومان</Text>
                                        <Text style={styles.label}>مبلغ تخفیف بیمه: </Text>
                                    </View>
                                    <View style={styles.middleItem}>
                                        <Text style={styles.label}>25563 تومان</Text>
                                        <Text style={styles.label}>مبلغ پایه بیمه: </Text>
                                    </View>
                                </View>
                                <View style={styles.bottom}>
                                    <TouchableOpacity onPress={() => Actions.home()} style={styles.advertise}>
                                        <Text style={styles.buttonTitle}>سفارش</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            : null
                    }
                </View>
            </View>
        );
    }
}
export default InsuranceInfo;