import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    reduction: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
        paddingTop: 15,
        paddingBottom: 15
    },
    redlabel: {
        color: 'rgba(122, 130, 153, 1)',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14

    },
});