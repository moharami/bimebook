import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    insuranceInfoContainer: {
        width: '100%',
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 15
    },
    labelContainer: {
        paddingRight: 10
    },
    title: {
        fontSize: 15,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    startContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    rowContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    branchContainer: {
        paddingRight: 7,
        paddingLeft: 7,
        paddingTop: 3,
        paddingBottom: 3,
        backgroundColor: 'rgb(168, 170, 173)',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 15
    },
    branchTxt: {
        fontSize: 13,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)'
    }
});
