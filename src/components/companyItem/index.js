
import React, {Component} from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {Actions} from 'react-native-router-flux'
import headerImg from '../../assets/bimebook-logo-home.png'
import asia from '../../assets/asia.png'
import SIcon from 'react-native-vector-icons/dist/SimpleLineIcons';

class CompanyItem extends Component {
    constructor(props){
        super(props);
        this.state = {
        };
    }
    render() {
        return (
            <View style={styles.insuranceInfoContainer}>
                <View style={styles.rowContainer}>
                    <SIcon name="arrow-left" size={14} color="gray" />
                    <View style={styles.branchContainer}>
                        <Text style={styles.branchTxt}>تعداد شعب</Text>
                        <Text style={styles.branchTxt}>128</Text>
                    </View>
                </View>
                <View style={styles.rowContainer}>
                    <View style={styles.labelContainer}>
                        <Text style={styles.title}>بیمه آسیا</Text>
                        <View style={styles.startContainer}>
                            <Icon name="star" size={14} color="rgba(255, 193, 39, 1)" />
                            <Icon name="star" size={14} color="rgba(255, 193, 39, 1)" />
                            <Icon name="star" size={14} color="rgba(255, 193, 39, 1)" />
                        </View>
                    </View>
                    <Image source={asia} style={{width: 50, height: 50, resizeMode: 'contain'}} />
                </View>
            </View>
        );
    }
}
export default CompanyItem;