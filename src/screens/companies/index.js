
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput, ImageBackground,  Dimensions} from 'react-native';
import styles from './styles'
import headerBg from '../../assets/headerBg.png'
import {Actions} from 'react-native-router-flux'
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bani.azarinpro.info/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import PageHeader from '../../components/pageHeader'
import FooterMenu from '../../components/footerMenu'
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import AppSearchBar from '../../components/searchBar'
import CompanyItem from "../../components/companyItem/index";

class Companies extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            defaultItem: 2,
            area_price: 1,
            sliderChange: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        } else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.topHeader}>
                            <PageHeader title="شرکت های بیمه" />
                        </View>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            <AppSearchBar />
                            <CompanyItem/>
                            <CompanyItem/>
                            <CompanyItem/>
                        </View>
                    </ScrollView>
                    <FooterMenu active="company" />
                </View>
            );
    }
}
export default Companies;

