
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput, ImageBackground,Animated,  Dimensions} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bani.azarinpro.info/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import PageHeader from '../../components/pageHeader'
import FooterMenu from '../../components/footerMenu'
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
import FIcon from 'react-native-vector-icons/dist/Feather';
import asia from '../../assets/asia.png'
import InfoItem from "../../components/infoItem/index";
import ImagePicker from 'react-native-image-picker'

class InstalmentExport extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            first: '',
            src1: null,
            src2: null
        };
        this.onBackPress = this.onBackPress.bind(this);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    selectPhotoTapped(id) {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                if(id === 1){
                    this.setState({
                        src1: response.uri
                    })
                }
                else if(id === 2){
                    this.setState({
                        src2: response.uri
                    })
                }
            }
        });
    }
    render() {
        console.log(this.state.src1 !== null )
        if(this.state.loading){
            return (<Loader />)
        } else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.topHeader}>
                            <PageHeader carInfo={false} title="درخواست صدور" />
                        </View>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            <Text style={styles.bodyLabel}>مبلغ پیش پرداخت</Text>
                            <View style={styles.preContainer}>
                                <Text style={styles.preText}>899,000 تومان</Text>
                            </View>
                            <Text style={styles.bodyLabel}>قسط اول</Text>
                            <View style={styles.insurancerContainer}>
                                <View style={[styles.insuranceRow, {marginBottom: 15}]}>
                                    <InfoItem label="تاریخ سررسید" value="" yellow />
                                    <InfoItem label="مبلغ" value="" yellow />
                                </View>
                                {
                                    this.state.src1 ? <Text style={styles.inputLabel}>تصویر چک قسط اول</Text> : null

                                }
                                <View style={{position: 'relative', zIndex: 1, width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                    <TouchableOpacity onPress={() => this.selectPhotoTapped(1)} style={{position: 'absolute', zIndex: 90, top: '24%', left: '10%'}}>
                                        <FIcon name="camera" size={20} color="rgb(11, 81, 144)" />
                                    </TouchableOpacity>
                                    <TextInput
                                        placeholderTextColor={'gray'}
                                        placeholder={this.state.src1 !== null ? "image.jpg" : "تصویر چک قسط اول"}
                                        underlineColorAndroid='transparent'
                                        value={this.state.src1 !== null ? "image.jpg" : "تصویر چک قسط اول"}
                                        style={{
                                            textAlign: 'right',
                                            borderWidth: 1,
                                            borderColor: 'rgb(207, 207, 207)',
                                            height: 40,
                                            backgroundColor: 'white',
                                            paddingRight: 10,
                                            paddingLeft: 10,
                                            width: '95%',
                                            borderRadius: 25,
                                            fontSize: 12,
                                            color: '#7A8299',
                                            borderStyle: 'dashed'
                                        }}
                                        onChangeText={(text) => {
                                            this.setState({first: text})
                                        }}/>
                                </View>
                            </View>
                            <Text style={styles.bodyLabel}>قسط دوم</Text>
                            <View style={styles.insurancerContainer}>
                                <View style={[styles.insuranceRow, {marginBottom: 15}]}>
                                    <InfoItem label="تاریخ سررسید" value="" yellow />
                                    <InfoItem label="مبلغ" value="" yellow />
                                </View>
                                {
                                    this.state.src2 ? <Text style={styles.inputLabel}>تصویر چک قسط دوم</Text> : null

                                }
                                <View style={{position: 'relative', zIndex: 1, width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                    <TouchableOpacity onPress={() => this.selectPhotoTapped(2)} style={{position: 'absolute', zIndex: 90, top: '24%', left: '10%'}}>
                                        <FIcon name="camera" size={20} color="rgb(11, 81, 144)" />
                                    </TouchableOpacity>
                                    <TextInput
                                        placeholderTextColor={'gray'}
                                        placeholder={this.state.src2 !== null ? "image.jpg" : "تصویر چک قسط دوم"}
                                        underlineColorAndroid='transparent'
                                        value={this.state.src2 !== null ? "image.jpg" : "تصویر چک قسط دوم"}
                                        style={{
                                            textAlign: 'right',
                                            borderWidth: 1,
                                            borderColor: 'rgb(207, 207, 207)',
                                            height: 40,
                                            backgroundColor: 'white',
                                            paddingRight: 10,
                                            paddingLeft: 10,
                                            width: '95%',
                                            borderRadius: 25,
                                            fontSize: 12,
                                            color: '#7A8299',
                                            borderStyle: 'dashed'
                                        }}
                                        onChangeText={(text) => {
                                            this.setState({first: text})
                                        }}/>
                                </View>
                            </View>
                            <TouchableOpacity onPress={()=> Actions.home()} style={styles.reminder}>
                                <Text style={styles.reminderText}>تایید و پرداخت</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                    <FooterMenu active="home" />
                </View>
            );
    }
}
export default InstalmentExport;

