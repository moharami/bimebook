
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput, ImageBackground,  Dimensions} from 'react-native';
import styles from './styles'
import tag from '../../assets/tag.png'
import {Actions} from 'react-native-router-flux'
import FIcon from 'react-native-vector-icons/dist/Feather';
import OIcon from 'react-native-vector-icons/dist/Octicons';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bani.azarinpro.info/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import PageHeader from '../../components/pageHeader'
import FooterMenu from '../../components/footerMenu'
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/dist/FontAwesome';

class CustomerInfo extends Component {

    constructor(props){
        super(props);
        this.state = {
            loading: false,
            name: '',
            tel: '',
            reciver: '',
            time: '',
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        } else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.topHeader}>
                            <PageHeader carInfo={false} title="مشخصات تحویل گیرنده" />
                        </View>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            <Text style={styles.label}>اطلاعات محلی که بایستی بیمه نامه به آنجا ارسال گردد را وارد نمایید:</Text>
                            {
                                this.state.name.length !== 0 ?  <Text style={styles.inputLabel}>نام تحویل گیرنده</Text> : null

                            }
                            <TextInput
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                placeholder='نام تحویل گیرنده'
                                value={this.state.name}
                                style={{
                                    textAlign: 'right',
                                    borderWidth: 1,
                                    borderColor: 'rgb(207, 207, 207)',
                                    height: 50,
                                    backgroundColor: 'white',
                                    paddingRight: 10,
                                    paddingLeft: 10,
                                    width: '87%',
                                    borderRadius: 25,
                                    fontSize: 16,
                                    color: '#7A8299',
                                    marginBottom: 20
                                }}
                                onChangeText={(text) => {
                                    this.setState({name: text})

                                }}/>
                            {
                                this.state.tel.length !== 0 ?  <Text style={styles.inputLabel}>شماره تماس</Text> : null

                            }
                            <TextInput
                                placeholderTextColor={'gray'}
                                placeholder={'شماره تماس'}
                                underlineColorAndroid='transparent'
                                value={this.state.tel}
                                style={{
                                    textAlign: 'right',
                                    borderWidth: 1,
                                    borderColor: 'rgb(207, 207, 207)',
                                    height: 50,
                                    backgroundColor: 'white',
                                    paddingRight: 10,
                                    paddingLeft: 10,
                                    width: '87%',
                                    borderRadius: 25,
                                    fontSize: 16,
                                    color: '#7A8299',
                                    borderStyle: 'dashed',
                                    marginBottom: 20
                                }}
                                onChangeText={(text) => {
                                    this.setState({tel: text})

                                }}/>
                            {
                                this.state.reciver.length !== 0 ?  <Text style={styles.inputLabel}>نشانی گیرنده</Text> : null

                            }
                            <TextInput
                                placeholderTextColor={'gray'}
                                placeholder={'نشانی گیرنده'}
                                underlineColorAndroid='transparent'
                                value={this.state.reciver}
                                style={{
                                    textAlign: 'right',
                                    borderWidth: 1,
                                    borderColor: 'rgb(207, 207, 207)',
                                    height: 50,
                                    backgroundColor: 'white',
                                    paddingRight: 10,
                                    paddingLeft: 10,
                                    width: '87%',
                                    borderRadius: 25,
                                    fontSize: 16,
                                    color: '#7A8299',
                                    borderStyle: 'dashed',
                                    marginBottom: 20
                                }}
                                onChangeText={(text) => {
                                    this.setState({reciver: text})

                                }}/>
                            {
                                this.state.time.length !== 0 ?  <Text style={styles.inputLabel}>زمان مراجعه</Text> : null

                            }
                            <TextInput
                                placeholderTextColor={'gray'}
                                placeholder={'زمان مراجعه'}
                                underlineColorAndroid='transparent'
                                value={this.state.time}
                                style={{
                                    textAlign: 'right',
                                    borderWidth: 1,
                                    borderColor: 'rgb(207, 207, 207)',
                                    height: 50,
                                    backgroundColor: 'white',
                                    paddingRight: 10,
                                    paddingLeft: 10,
                                    width: '87%',
                                    borderRadius: 25,
                                    fontSize: 16,
                                    color: '#7A8299',
                                    borderStyle: 'dashed',
                                    marginBottom: 20
                                }}
                                onChangeText={(text) => {
                                    this.setState({time: text})

                                }}/>
                            <TouchableOpacity onPress={() => null}>
                                <Text style={styles.mapText}>ثبت نشانی بر روی نقشه(اختیاری)</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => Actions.home()} style={styles.advertise}>
                                <Text style={styles.buttonTitle}>ثبت سفارش</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                    <View style={styles.reminder}>
                        <Icon name="bell-o" size={25} color="white" />
                        <Text style={styles.reminderText}>یادآور بیمه</Text>
                    </View>
                    <FooterMenu active="home" />
                </View>
            );
    }
}
export default CustomerInfo;

