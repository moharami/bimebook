

import { StyleSheet, Dimensions} from 'react-native';
// import env from '../../colors/env';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(246, 246, 246)',
        // backgroundColor: 'red'
    },
    header: {
        flex: 1,
        height: '22%',
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9990
    },
    topHeader: {
        backgroundColor: 'rgb(20, 85, 151)',
        width: '100%',
        height: '45%'
    },
    headerImage: {
        width: '100%'
    },
    headerTitle: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: '19%'
    },
    bodyContainer: {
        width: '100%',
        paddingBottom: 180,
        paddingTop: 50,
        backgroundColor: 'rgb(248, 248, 248)',
        alignItems: 'center',
        justifyContent: 'center',
    },
    scroll: {
        paddingTop: 45,
        backgroundColor: 'rgb(249, 249, 249)'
    },
    iconContainer: {
        width: 30,
        height: 30,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    tagImageContainer: {
        position: 'relative'
    },
    tagTxt1: {
        position: 'absolute',
        top: '5%',
        left: '35%',
        fontSize: 35,
        fontFamily: 'IRANSansMobile(FaNum)_Bold',
        color: 'black'
    },
    tagTxt2: {
        position: 'absolute',
        top: '5%',
        left: '25%',
        fontSize: 35,
        fontFamily: 'IRANSansMobile(FaNum)_Bold',
        color: 'black'

    },
    tagTxt3: {
        position: 'absolute',
        top: '5%',
        left: '12%',
        fontSize: 35,
        fontFamily: 'IRANSansMobile(FaNum)_Bold',
        color: 'black'

    },
    tagTxt4: {
        position: 'absolute',
        top: '5%',
        right: '3%',
        fontSize: 40,
        fontFamily: 'IRANSansMobile(FaNum)_Bold',
        color: 'gray'
    },
    picker: {
        height: 20,
        // backgroundColor: 'white',
        width: '100%',
        color: 'gray',
        // elevation: 4,
        borderRadius: 10,
        // marginBottom: 20
        // overflow: 'hidden'
    },
    label: {
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        padding: 15
    },
    inputLabel: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        paddingRight: '10%',
        paddingBottom: 5
    },
    reminder: {
        position: 'absolute',
        right: 20,
        bottom: 85,
        zIndex: 40,
        backgroundColor: '#fdb913',
        borderRadius: 50,
        padding: 13,
        alignItems: 'center',
        justifyContent: 'center'
    },
    reminderText: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    headerContainer: {
        alignItems: 'flex-end',
        justifyContent: 'center',
        padding: 15,
        borderBottomWidth: 1,
        borderBottomColor: 'rgb(237, 237, 237)',
        backgroundColor: 'white'
    },
    headerText: {
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black',
        alignSelf: 'flex-end'
    },
    vacleContainer: {
        width: '85%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    vacle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingRight: 15,
        paddingLeft: 15,
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 10,

    },
    vacleText: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 13,
        paddingRight: 10
    },
    txt: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 13,
        paddingRight: 15,
        color: 'black',
        paddingTop: 10,
        paddingBottom: 10,
    },
    itemContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 25
    },
    innerPickerContainer: {
        width: '48%',
        height: 40,
        borderRadius: 20,
        borderWidth: 1,
        borderColor: 'rgb(200, 200, 200)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    pickerContainer: {
        width: '90%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    advertise: {
        width: '40%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#8dc63f',
        padding: 8,
        marginTop: 35
    },
    buttonTitle: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    insuranceContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 20
    },
    mapText: {
        fontSize: 14,
        color: 'rgb(11, 81, 144)',
        fontFamily: 'IRANSansMobile(FaNum)',
        textDecorationLine: 'underline'
    }
});

