
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput, ImageBackground,  Dimensions} from 'react-native';
import styles from './styles'
import headerBg from '../../assets/headerBg.png'
import {Actions} from 'react-native-router-flux'
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bani.azarinpro.info/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import InfoItem from '../../components/infoItem'
import FooterMenu from '../../components/footerMenu'
import user from '../../assets/user.png'
import TransactionBox from "../../components/transactionBox/index";
import Address from "../../components/address";
import ImagePicker from 'react-native-image-picker'
import EditModal from './editModal'
import OptionalEditModal from './optionalEditModal'

class Profile extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            nationalCard: '',
            third: '',
            newDoc: '',
            src1: null,
            src2: null,
            src3: null,
            editModal: false,
            optionalModal: false,
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

    }
    selectPhotoTapped(id) {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                if(id == 1){
                    this.setState({
                        src1: response.uri
                    })
                }
                else if(id === 2){
                    this.setState({
                        src2: response.uri
                    })
                }
                else if (id === 3){
                    this.setState({
                        src3: response.uri
                    })
                }
            }
        });
    }
    closeModalEdit() {
        this.setState({editModal: false});
    }
    closeModalOptional() {
        this.setState({optionalModal: false});
    }

    setModalVisibleEdit() {
        this.setState({editModal: !this.state.editModal});
    }
    setModalVisibleOptional() {
        this.setState({optionalModal: !this.state.optionalModal});
    }
    render() {

        if(this.state.loading){
            return (<Loader />)
        } else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.topHeader}>
                            <View style={styles.headerTitleContainer}>
                                <TouchableOpacity onPress={() => this.onBackPress()}>
                                    <FIcon name="chevron-left" size={30} color="white" />
                                </TouchableOpacity>
                                <Text style={styles.profileTitle}>پروفایل</Text>
                            </View>
                        </View>
                        <Image source={headerBg} style={styles.headerImage} />
                    </View>
                    <View style={styles.imageContainer}>
                        <Image source={user} style={styles.Image} />

                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            <View style={styles.body}>
                                <Text style={styles.label}>سلام سعید خوش اومدی !</Text>
                                <TouchableOpacity onPress={() => null} style={styles.exitContainer}>
                                    <Text style={styles.exitLabel}>خروج</Text>
                                </TouchableOpacity>
                                <View style={styles.labelContainer}>
                                    <TouchableOpacity onPress={() => this.setState({ editModal: true})} style={styles.editContainer}>
                                        <Text style={styles.editText}>ویرایش</Text>
                                        <FIcon name="edit" size={20} color="rgb(12, 85, 142)" />
                                    </TouchableOpacity>
                                    <Text style={styles.title}>مشخصات</Text>
                                </View>
                                <View style={styles.insurancerContainer}>
                                    <View style={[styles.insuranceRow, {marginBottom: 6}]}>
                                        <InfoItem label="نام خانوادگی" value="" />
                                        <InfoItem label="نام" value="" />
                                    </View>
                                    <View style={styles.insuranceRow}>
                                        <InfoItem label="موبایل" value="" />
                                        <InfoItem label="کد ملی" value="" />
                                    </View>
                                    <View style={[styles.insuranceRow, {marginTop: 7}]}>
                                        <InfoItem label="ایمیل" value="" long />
                                    </View>
                                </View>
                                <View style={styles.labelContainer}>
                                    <TouchableOpacity onPress={() => this.setState({ optionalModal: true})} style={styles.editContainer}>
                                        <Text style={styles.editText}>ویرایش</Text>
                                        <FIcon name="edit" size={20} color="rgb(12, 85, 142)" />
                                    </TouchableOpacity>
                                    <Text style={styles.title}>مشخصات اختیاری</Text>
                                </View>
                                <View style={styles.insurancerContainer}>
                                    <View style={[styles.insuranceRow, {marginBottom: 6}]}>
                                        <InfoItem label="تاریخ تولد" value="" />
                                        <InfoItem label="جنسیت" value="" />
                                    </View>
                                    <View style={[styles.insuranceRow, {justifyContent: 'flex-end'}]}>
                                        <InfoItem label="تحصیلات" value="" />
                                    </View>
                                </View>
                                <Text style={[styles.title, {alignSelf: 'flex-end', paddingTop: 40, padding: 10}]}>آدرس ها</Text>
                                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ transform: [{ scaleX: -1}]}}>
                                    <View style={styles.slideContainers}>
                                        <TouchableOpacity onPress={() => null} style={styles.addContainers}>
                                            <FIcon name="plus" size={30} color="rgb(12, 85, 142)" />
                                            <Text style={styles.addText}>آدرس جدید</Text>
                                        </TouchableOpacity>
                                        <Address />
                                        <Address />
                                    </View>
                                </ScrollView>
                                <Text style={[styles.title, {alignSelf: 'flex-end', paddingTop: 40, padding: 10}]}>مدارک</Text>
                                {
                                    this.state.src1 ?  <Text style={styles.inputLabel}>کارت ملی</Text> : null

                                }
                                <View style={{position: 'relative', zIndex: 1, width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                    <TouchableOpacity onPress={() => this.selectPhotoTapped(1)} style={{position: 'absolute', zIndex: 90, top: '22%', left: '15%'}}>
                                        <FIcon name="camera" size={20} color="rgb(11, 81, 144)" />
                                    </TouchableOpacity>
                                    <TextInput
                                        placeholderTextColor={'gray'}
                                        underlineColorAndroid='transparent'
                                        placeholder={this.state.src1 !== null ? "image.jpg" : "کارت ملی"}
                                        value={this.state.src1 !== null ? "image.jpg" : "کارت ملی"}
                                        style={{
                                            textAlign: 'right',
                                            borderWidth: 1,
                                            borderColor: 'rgb(207, 207, 207)',
                                            height: 50,
                                            backgroundColor: this.state.src1 !== null ? "white" : "transparent",
                                            paddingRight: 10,
                                            paddingLeft: 10,
                                            width: '90%',
                                            borderRadius: 25,
                                            fontSize: 14,
                                            color: '#7A8299',
                                            marginBottom: 20
                                        }}
                                        onChangeText={(text) => {
                                            this.setState({nationalCard: text})

                                        }}/>
                                </View>
                                {
                                    this.state.src2 ?  <Text style={styles.inputLabel}>بیمه نامه شخص ثالث</Text> : null

                                }
                                <View style={{position: 'relative', zIndex: 1, width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                    <TouchableOpacity onPress={() => this.selectPhotoTapped(2)} style={{position: 'absolute', zIndex: 90, top: '22%', left: '15%'}}>
                                        <FIcon name="camera" size={20} color="rgb(11, 81, 144)" />
                                    </TouchableOpacity>
                                    <TextInput
                                        placeholderTextColor={'gray'}
                                        underlineColorAndroid='transparent'
                                        placeholder={this.state.src2 !== null ? "image.jpg" : "بیمه نامه شخص ثالث"}
                                        value={this.state.src2 !== null ? "image.jpg" : "بیمه نامه شخص ثالث"}
                                        style={{
                                            textAlign: 'right',
                                            borderWidth: 1,
                                            borderColor: 'rgb(207, 207, 207)',
                                            height: 50,
                                            backgroundColor: this.state.src2 !== null ? "white" : "transparent",
                                            paddingRight: 10,
                                            paddingLeft: 10,
                                            width: '90%',
                                            borderRadius: 25,
                                            fontSize: 14,
                                            color: '#7A8299',
                                            marginBottom: 20
                                        }}
                                        onChangeText={(text) => {
                                            this.setState({third: text})

                                        }}/>
                                </View>
                                <Text style={[styles.title, {alignSelf: 'flex-end', paddingTop: 40, padding: 10}]}>مدرک جدید</Text>
                                <View style={styles.innerPickerContainer}>
                                    <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                        <FIcon name="chevron-down" size={20} color="black" style={{position: 'absolute', zIndex: 90, top: 4, left: 7}}/>
                                        <Picker
                                            mode="dropdown"
                                            style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                            selectedValue={this.state.name}
                                            // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                            onValueChange={(itemValue) =>{
                                                this.setState({
                                                    vacleType: itemValue
                                                })}}>
                                            <Picker.Item label="عنوان مدرک" value={0} />
                                            <Picker.Item  label='sdew' value={1} />
                                        </Picker>
                                    </View>
                                </View>
                                {
                                    this.state.src3 ?  <Text style={styles.inputLabel}>ارسال مدرک جدید</Text> : null

                                }
                                <View style={{position: 'relative', zIndex: 1, width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                    <TouchableOpacity onPress={() => this.selectPhotoTapped(3)} style={{position: 'absolute', zIndex: 90, top: '22%', left: '15%'}}>
                                        <FIcon name="camera" size={20} color="rgb(11, 81, 144)" />
                                    </TouchableOpacity>
                                    <TextInput
                                        placeholderTextColor={'gray'}
                                        underlineColorAndroid='transparent'
                                        placeholder={this.state.src3 !== null ? "image.jpg" : "ارسال مدرک جدید "}
                                        value={this.state.src3 !== null ? "image.jpg" : "ارسال مدرک جدید"}
                                        style={{
                                            textAlign: 'right',
                                            borderWidth: 1,
                                            borderColor: 'rgb(207, 207, 207)',
                                            height: 50,
                                            backgroundColor: this.state.src3 !== null ? "white" : "transparent",
                                            paddingRight: 10,
                                            paddingLeft: 10,
                                            width: '90%',
                                            borderRadius: 25,
                                            fontSize: 14,
                                            color: '#7A8299',
                                            marginBottom: 20
                                        }}
                                        onChangeText={(text) => {
                                            this.setState({newDoc: text})
                                        }}/>
                                </View>
                                <TouchableOpacity onPress={() => null} style={styles.advertise}>
                                    <Text style={styles.buttonTitle}>تایید</Text>
                                </TouchableOpacity>
                            </View>
                            <EditModal
                                closeModal={() => this.closeModalEdit()}
                                modalVisible={this.state.editModal}
                                onChange={(visible) => this.setModalVisibleEdit(visible)}
                            />
                            <OptionalEditModal
                                closeModal={() => this.closeModalOptional()}
                                modalVisible={this.state.optionalModal}
                                onChange={(visible) => this.setModalVisibleOptional(visible)}
                            />
                        </View>
                    </ScrollView>
                    <FooterMenu active="home" />
                </View>
            );
    }
}
export default Profile;

