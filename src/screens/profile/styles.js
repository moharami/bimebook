
import { StyleSheet, Dimensions} from 'react-native';
// import env from '../../colors/env';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(246, 246, 246)',
        // backgroundColor: 'red'
    },
    headerTitleContainer: {
       flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingTop: 20
    },
    header: {
        flex: 1,
        height: '25%',
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9990
    },
    headerImage: {
        width: '100%'
    },
    imageContainer: {
        position: 'absolute',
        zIndex: 9999,
        borderRadius: 100,
        top: '13%',
        left: '35%',
        padding: 20,
        backgroundColor: 'rgb(168, 170, 173)'
    },
    Image: {
        width: 60,
        height: 60,
        tintColor: 'white'
    },
    topHeader: {
        backgroundColor: 'rgb(20, 85, 151)',
        width: '100%',
        height: '73%'
    },
    headerTitle: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: '19%'
    },
    profileTitle: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingLeft: '37%'
    },
    bodyContainer: {
        width: '100%',
        paddingBottom: 180,
        // paddingRight: 10,
        // paddingLeft: 10,
        paddingTop: 30
    },
    scroll: {
        paddingTop: 120,
        paddingRight: 15,
        paddingLeft: 15,

    },
    body : {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-around',
        // padding: 10,

    },
    label: {
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: 30
    },
    item: {
        width: '30%',
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 13
    },
    exitContainer: {
        backgroundColor: 'red',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        paddingLeft: 15,
        paddingRight: 15,
        marginTop: 10
    },
    exitLabel: {
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'white'
    },
    labelContainer: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 8,
        paddingTop: 45
    },
    editContainer: {
        flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent: 'flex-start'
    },
    editText: {
        color: 'rgb(12, 85, 142)',
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingRight: 10
    },
    title: {
        color: 'black',
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    inputLabel: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        paddingRight: '10%',
        paddingBottom: 5
    },
    insurancerContainer: {
        width: '100%',
        padding: 6,
        backgroundColor: 'white',
        borderRadius: 10,
        elevation: 20
    },
    insuranceRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    slideContainers: {
        flexDirection: 'row',
        transform: [
            {rotateY: '180deg'},
        ],
    },
    addContainers: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        borderRadius: 10,
        paddingRight: 20,
        paddingLeft: 20
    },
    addText: {
        color: 'rgb(12, 85, 142)',
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    picker: {
        height: 40,
        backgroundColor: 'rgb(246, 246, 246)',
        width: '100%',
        color: 'rgb(100,100,100)',
        borderColor: 'lightgray',
        borderWidth: 1,
        // elevation: 4,
        borderRadius: 10,
        marginBottom: 30,
        // overflow: 'hidden'
    },
    innerPickerContainer: {
        width: '90%',
        height: 50,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: 'rgb(200, 200, 200)',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20
    },
    advertise: {
        width: '35%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#8dc63f',
        padding: 5,
        marginBottom: 30
    },
    buttonTitle: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
});

