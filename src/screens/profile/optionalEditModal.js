
import React, { Component } from 'react';
import {
    Modal,
    Text,
    View,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    AsyncStorage,
    Linking,
    Alert
}
    from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome'

import Axios from 'axios'
// Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
// export const url = 'http://fitclub.ws';
// Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import InsuranceBox from "../../components/insuranceBox";
import moment_jalaali from 'moment-jalaali'
import PrsianCalendarPicker from 'react-native-persian-calendar-picker';

class OptionalEditModal extends Component {
    state = {
        modalVisible: false,
        time: '',
        company: '',
        fname: '',
        lname: '',
        nationalId: '',
        mobile: '',
        email: '',
        sex: '',
        education: '',
        birthday: '',
        showPicker:false,
        selectedStartDate:null
    };
    onDateChange(date) {
        setTimeout(() => {this.setState({showPicker: false})}, 200)
        this.setState({ selectedStartDate: date });
    }
    render() {
        if(this.state.loading)
            return (<Loader txtColor="red" color='red' />);
        else return (
            <View style = {styles.container}>
                <Modal animationType = {"fade"} transparent = {true}
                       visible = {this.props.modalVisible}
                       onRequestClose = {() => this.props.onChange(false)}
                       onBackdropPress= {() => this.props.onChange(!this.state.modalVisible)}
                >
                    <View style = {styles.modal}>
                        <View style={styles.box}>
                            <View style={styles.headerContainer}>
                                <TouchableOpacity onPress={() => this.props.closeModal()}>
                                    <Icon name="close" size={20} color={ "black"} />
                                </TouchableOpacity>
                                <Text style={styles.headerText}>ویرایش مشخصات اختیاری</Text>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.itemContainer}>
                                    {
                                        this.state.lname.length !== 0 ? <Text style={styles.inputLabel}>تاریخ تولد</Text> : null
                                    }
                                    <TextInput
                                        onFocus={() =>{
                                            this.setState({
                                                showPicker: true
                                            })}}
                                        placeholderTextColor={'gray'}
                                        underlineColorAndroid='transparent'
                                        placeholder='تاریخ تولد'
                                        value={this.state.selectedStartDate !==null ? moment_jalaali(this.state.selectedStartDate).format('jYYYY/jM/jD') :null}
                                        style={{
                                            textAlign: 'right',
                                            borderWidth: 1,
                                            borderColor: 'rgb(100, 100, 100)',
                                            height: 35,
                                            backgroundColor: 'white',
                                            paddingRight: 10,
                                            paddingLeft: 10,
                                            width: '100%',
                                            borderRadius: 25,
                                            fontSize: 13,
                                            color: 'black',
                                            marginBottom: 8
                                        }}
                                        onChangeText={(text) => {
                                            this.setState({lname: text})
                                        }}/>
                                </View>
                                <View style={styles.itemContainer}>
                                    {
                                        this.state.sex.length !== 0 ? <Text style={styles.inputLabel}>جنسیت</Text> : null
                                    }
                                    <TextInput
                                        placeholderTextColor={'gray'}
                                        underlineColorAndroid='transparent'
                                        placeholder='جنسیت'
                                        value={this.state.sex}
                                        style={{
                                            textAlign: 'right',
                                            borderWidth: 1,
                                            borderColor: 'rgb(100, 100, 100)',
                                            height: 35,
                                            backgroundColor: 'white',
                                            paddingRight: 10,
                                            paddingLeft: 10,
                                            width: '100%',
                                            borderRadius: 25,
                                            fontSize: 13,
                                            color: 'black',
                                            marginBottom: 8
                                        }}
                                        onChangeText={(text) => {
                                            this.setState({sex: text})
                                        }}/>
                                </View>
                            </View>
                            <View style={[styles.row, {justifyContent: 'flex-end'}]}>
                                <View style={styles.itemContainer}>
                                    {
                                        this.state.education.length !== 0 ? <Text style={styles.inputLabel}>تحصیلات</Text> : null
                                    }
                                    <TextInput
                                        placeholderTextColor={'gray'}
                                        underlineColorAndroid='transparent'
                                        placeholder='تحصیلات'
                                        value={this.state.education}
                                        style={{
                                            textAlign: 'right',
                                            borderWidth: 1,
                                            borderColor: 'rgb(100, 100, 100)',
                                            height: 35,
                                            backgroundColor: 'white',
                                            paddingRight: 10,
                                            paddingLeft: 10,
                                            width: '100%',
                                            borderRadius: 25,
                                            fontSize: 13,
                                            color: 'black',
                                            marginBottom: 8
                                        }}
                                        onChangeText={(text) => {
                                            this.setState({education: text})
                                        }}/>
                                </View>
                            </View>
                            <TouchableOpacity onPress={() => null} style={styles.advertise}>
                                <Text style={styles.buttonTitle}>تایید</Text>
                            </TouchableOpacity>
                        </View>
                        {
                            this.state.showPicker ?
                                <View style={{
                                    position: 'absolute',
                                    bottom: '13%',
                                    zIndex: 9999,
                                    backgroundColor: 'white'
                                }}>
                                    <PrsianCalendarPicker
                                        onDateChange={(date) => this.onDateChange(date)}
                                    />
                                </View>
                                : null
                        }
                    </View>
                </Modal>
            </View>
        )
    }
}
export default OptionalEditModal

const styles = StyleSheet.create ({
    container: {
        alignItems: 'center',
        // backgroundColor: 'white',
        padding: 10,
        position: 'absolute',
        bottom: -200
    },
    modal: {
        position: 'relative',
        zIndex: 0,
        flexGrow: 1,
        justifyContent: 'center',
        // flex: 1,
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,.6)',
        paddingRight: 20,
        paddingLeft: 20
    },
    text: {
        color: '#3f2949',
        marginTop: 10
    },
    box: {
        width: '100%',
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        borderRadius: 15,
        paddingBottom: 20
    },
    input: {
        width: '100%',
        fontSize: 16,
        paddingTop: 0,
        textAlign: 'right',
    },
    searchButton: {
        width: '90%',
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgb(20, 122, 170)'
    },
    searchText:{
        color: 'white'
    },
    picker: {
        height:50,
        width: "100%",
        alignSelf: 'flex-end'
    },
    label: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'rgb(60, 60, 60)'
    },
    headerContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 8,
        borderBottomWidth: 1,
        borderBottomColor: 'rgb(237, 237, 237)',
        marginBottom: 20
    },
    headerText: {
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    advertise: {
        width: '35%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#8dc63f',
        padding: 5,
        // marginTop: 0
    },
    buttonTitle: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    inputLabel: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        paddingRight: '10%',
        paddingBottom: 5
    },
    insContainer: {
        width: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        // alignItems: 'center',
        // justifyContent: 'space-between',
        padding: 8
    },
    row: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    itemContainer: {
        width: '48%'
    }
})