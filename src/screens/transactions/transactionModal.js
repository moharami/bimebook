
import React, { Component } from 'react';
import {
    Modal,
    Text,
    View,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    AsyncStorage,
    Linking,
    Alert
}
    from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome'

import Axios from 'axios';
// Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
// export const url = 'http://fitclub.ws';
// Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import MIcon from 'react-native-vector-icons/dist/MaterialCommunityIcons'
import EIcon from 'react-native-vector-icons/dist/EvilIcons'
import PersianCalendarPicker from 'react-native-persian-calendar-picker';
import InfoItem from '../../components/infoItem'

class TransactionModal extends Component {
    state = {
        modalVisible: false,
        mobile: '',
        time: '',
        showPicker: false,
        selectedStartDate: null
    }
    onDateChange(date) {
        setTimeout(() => {this.setState({showPicker: false})}, 100);
        this.setState({ selectedStartDate: date });
    }
    render() {
        if(this.state.loading)
            return (<Loader txtColor="red" color='red' />);
        else return (
            <View style = {styles.container}>
                <Modal animationType = {"fade"} transparent = {true}
                       visible = {this.props.modalVisible}
                       onRequestClose = {() => this.props.onChange(false)}
                       onBackdropPress= {() => this.props.onChange(!this.state.modalVisible)}
                >
                    <View style = {styles.modal}>
                        <View style={styles.box}>
                            <View style={styles.headerContainer}>
                                <TouchableOpacity onPress={() => this.props.closeModal()}>
                                    <Icon name="close" size={20} color={ "black"} />
                                </TouchableOpacity>
                                <Text style={styles.headerText}>جزئیات تراکنش</Text>
                            </View>
                            <View style={styles.insurancerContainer}>
                                <View style={[styles.insuranceRow, {marginBottom: 6}]}>
                                    <InfoItem label="شماره موبایل" value="" />
                                    <InfoItem label="نام تحویل گیرنده" value="" />
                                </View>
                                <View style={styles.insuranceRow}>
                                    <InfoItem label="نشانی:" value="" long />
                                </View>
                            </View>
                        </View>
                        {
                            this.state.showPicker ?
                                <View style={{
                                    position: 'absolute',
                                    bottom: '30%',
                                    zIndex: 9999,
                                    backgroundColor: 'white'
                                }}>
                                    <PersianCalendarPicker
                                        onDateChange={(date) => this.onDateChange(date)}
                                    />
                                </View>
                                : null
                        }
                    </View>
                </Modal>
            </View>
        )
    }
}
export default TransactionModal

const styles = StyleSheet.create ({
    container: {
        alignItems: 'center',
        // backgroundColor: 'white',
        padding: 10,
        position: 'absolute',
        bottom: -200
    },
    modal: {
        flexGrow: 1,
        justifyContent: 'center',
        // flex: 1,
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,.6)',
        paddingRight: 20,
        paddingLeft: 20

    },
    text: {
        color: '#3f2949',
        marginTop: 10
    },
    box: {
        position: 'relative',
        zIndex: 0,
        width: '100%',
        // height: 120,
        // backgroundColor: 'rgb(237, 237, 237)',
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        borderRadius: 15,
        paddingBottom: 20
    },
    input: {
        width: '100%',
        fontSize: 16,
        paddingTop: 0,
        textAlign: 'right',
    },
    searchButton: {
        width: '90%',
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgb(20, 122, 170)'
    },
    searchText:{
        color: 'white'
    },
    picker: {
        height:50,
        width: "100%",
        alignSelf: 'flex-end'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        marginBottom: 20

    },
    itemContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '20%'
    },
    iconContainer: {
        backgroundColor: 'white',
        width: 50,
        height: 50,
        borderRadius: 60,
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 6
    },
    label: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'rgb(60, 60, 60)'
    },
    headerContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 8,
        borderBottomWidth: 1,
        borderBottomColor: 'rgb(237, 237, 237)',
        marginBottom: 20
    },
    headerText: {
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    inputLabel: {
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'gray',
        alignSelf: 'flex-end'
    },
    advertise: {
        width: '40%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#8dc63f',
        padding: 8,
        // marginTop: 0
    },
    buttonTitle: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    insurancerContainer: {
        width: '100%',
        padding: 6,
        backgroundColor: 'white',
        borderRadius: 10
    },
    insuranceRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
})