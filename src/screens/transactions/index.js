
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput, ImageBackground,  Dimensions} from 'react-native';
import styles from './styles'
import headerBg from '../../assets/headerBg.png'
import {Actions} from 'react-native-router-flux'
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bani.azarinpro.info/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import PageHeader from '../../components/pageHeader'
import FooterMenu from '../../components/footerMenu'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import Transaction from "../../components/transaction";
import moment_jalaali from 'moment-jalaali'
import PersianCalendarPicker from 'react-native-persian-calendar-picker';
import TransactionModal from "./transactionModal";

class Transactions extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            defaultItem: 2,
            area_price: 1,
            sliderChange: false,
            active: 1,
            selectedStartDate: null,
            showPicker: false,
            transactionModal: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    onDateChange(date) {
        setTimeout(() => {this.setState({showPicker: false})}, 100);
        this.setState({ selectedStartDate: date });
    }
    setModalVisible() {
        this.setState({transactionModal: !this.state.transactionModal});
    }
    closeModal() {
        this.setState({transactionModal: false});

    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        } else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.topHeader}>
                            <PageHeader title="لیست کامل تراکنش ها" />
                        </View>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            {/*<AppSearchBar />*/}
                            <View style={styles.topContainer}>
                                <View style={styles.innerPickerContainer}>
                                    <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                        <TouchableOpacity onPress={() => this.setState({transactionModal: true})} style={{position: 'absolute', zIndex: 90, top: 6, left: 7}}>
                                            <FIcon name="chevron-down" size={20} color="gray" />
                                        </TouchableOpacity>
                                        <Picker
                                            mode="dropdown"
                                            style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                            selectedValue={this.state.name}
                                            // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                            onValueChange={(itemValue) =>{
                                                this.setState({
                                                    vacleType: itemValue
                                                })}}>
                                            <Picker.Item label="عنوان مدرک" value={0} />
                                            <Picker.Item  label='sdew' value={1} />
                                        </Picker>
                                    </View>
                                </View>
                                <View style={styles.innerPickerContainer}>
                                    <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                        <TouchableOpacity onPress={() => this.setState({  showPicker: true})} style={{position: 'absolute', zIndex: 90, top: 6, left: 7}}>
                                            <FIcon name="calendar" size={20} color="gray" />
                                        </TouchableOpacity>
                                        <TextInput
                                            // onFocus={() =>{
                                            //     this.setState({
                                            //         showPicker: true
                                            //     })}}
                                            placeholder='تاریخ سررسید'
                                            placeholderTextColor={'gray'}
                                            underlineColorAndroid='transparent'
                                            value={this.state.selectedStartDate !==null ? moment_jalaali(this.state.selectedStartDate).format('jYYYY/jM/jD') :null}
                                            style={{
                                                height: 36,
                                                marginBottom: 10,
                                                // paddingRight: 25,
                                                width: '100%',
                                                color: 'rgb(50, 50, 50)',
                                                fontSize: 14,
                                                textAlign: 'right'
                                            }}
                                        />
                                    </View>
                                </View>
                            </View>
                            <Text style={styles.title}>لیست تراکنش ها</Text>
                            <View style={styles.body}>
                                <TouchableOpacity onPress={() => this.setState({transactionModal: true})} style={{width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                    <Transaction />
                                </TouchableOpacity>
                            </View>
                            {
                                this.state.showPicker ?
                                    <View style={{
                                        position: 'absolute',
                                        bottom: '50%',
                                        zIndex: 9999,
                                        backgroundColor: 'white'
                                    }}>
                                        <PersianCalendarPicker
                                            onDateChange={(date) => this.onDateChange(date)}
                                        />
                                    </View>
                                    : null
                            }
                            <TransactionModal
                                closeModal={() => this.closeModal()}
                                modalVisible={this.state.transactionModal}
                                onChange={(visible) => this.setModalVisible(visible)}
                            />
                        </View>
                    </ScrollView>
                    <FooterMenu active="financial" showMore={true} />
                </View>
            );
    }
}
export default Transactions;

