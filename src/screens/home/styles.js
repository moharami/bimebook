
import { StyleSheet, Dimensions} from 'react-native';
// import env from '../../colors/env';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(246, 246, 246)',
        // backgroundColor: 'red'
    },
    header: {
        flex: 1,
        // flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent: 'space-around',
        // backgroundColor: 'white',
        height: '25%',
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9990
    },
    topHeader: {
        backgroundColor: 'rgb(20, 85, 151)',
        width: '100%',
        height: '73%'
    },
    headerImage: {
        width: '100%'
    },
    headerTitle: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: '19%'
    },
    bodyContainer: {
        width: '100%',
        paddingBottom: 180,
        // paddingRight: 10,
        // paddingLeft: 10,
        paddingTop: 30
    },
    scroll: {
        paddingTop: 120,
        paddingRight: 15,
        paddingLeft: 15,

    },
    body : {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-around',
        padding: 10,

    },
    row: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingBottom: 10
    },
    lastrow: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 10,
        // paddingRight: '10%',
        // paddingLeft: '10%',
    },
    iconContainer: {
        width: 30,
        height: 30,
        // borderRadius: 30,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    label: {
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingBottom: 10
    },
    item: {
        width: '30%',
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 13
    },
    imageContainer: {
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        // padding: 20,
        borderRadius: 13,
        elevation: 8
    },
    bodyImage: {
        width: 70,
        height: 70,
        resizeMode: 'contain',
        tintColor: 'rgb(20, 85, 151)'
    },
    bodyyImage: {
        width: 50,
        height: 50,
        resizeMode: 'contain',
        tintColor: 'rgb(20, 85, 151)',
        marginTop: 15,
        marginBottom: 15,
    },
    reminder: {
        position: 'absolute',
        right: 20,
        bottom: 85,
        zIndex: 40,
        backgroundColor: '#fdb913',
        borderRadius: 50,
        padding: 13,
        alignItems: 'center',
        justifyContent: 'center'
    },
    reminderText: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    }
});

