
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput, ImageBackground,  Dimensions} from 'react-native';
import styles from './styles'
import headerBg from '../../assets/headerBg.png'
import {Actions} from 'react-native-router-flux'
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bani.azarinpro.info/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import HomeHeader from '../../components/homeHeader'
import FooterMenu from '../../components/footerMenu'
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

import bimeh_tamin from '../../assets/insurance/bimeh-tamin.png'
import bimeh_darman from '../../assets/insurance/bimeh-darman.png'
import bimeh_mosaferat from '../../assets/insurance/bimeh-mosaferat.png'
import bimeh_masooliat from '../../assets/insurance/bimeh-masooliat.png'
import bimeh_zelzele from '../../assets/insurance/bimeh-zelzele.png'
import bimeh_atashsoozi from '../../assets/insurance/bimeh-atashsoozi.png'
import bimeh_omr from '../../assets/insurance/bimeh-omr.png'
import bimeh_motor from '../../assets/insurance/bimeh-motor.png'
import bimeh_badaneh from '../../assets/insurance/bimeh-badaneh.png'
import bimeh_sales from '../../assets/insurance/bimeh-sales.png'
import badge from '../../assets/badge.png'
import Icon from 'react-native-vector-icons/dist/FontAwesome';

class Home extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

    }
    render() {
        const times = this.state.times;
        // const {posts, categories, user} = this.props;
        if(this.state.loading){
            return (<Loader />)
        } else
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.topHeader}>
                        <HomeHeader />
                        <Text style={styles.headerTitle}>مشاوره، مقایسه و خرید آنلاین انواع بیمه</Text>
                    </View>
                    <Image source={headerBg} style={styles.headerImage} />
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <View style={styles.body}>
                            <Text style={styles.label}>نوع بیمه مورد نظر خود را انتخاب نمایید:</Text>
                            <View style={[styles.row, {position: 'relative', zIndex: 1}]}>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={() => Actions.vacleInfo({thirdBime: true,motor: true, pageTitle: 'بیمه موتور سیکلت'})} style={styles.imageContainer}>
                                        <Image source={bimeh_motor} style={styles.bodyImage} />
                                        <Text style={styles.label}>موتور سیکلت</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={() => Actions.vacleInfo({bodyBime: true, pageTitle: 'بیمه بدنه خودرو'})}  style={styles.imageContainer}>
                                        <Image source={bimeh_badaneh} style={styles.bodyImage} />
                                        <Text style={styles.label}>بدنه</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={() =>  Actions.vacleInfo({thirdBime: true, pageTitle: 'بیمه شخص ثالث'})} style={styles.imageContainer}>
                                        <Image source={bimeh_sales} style={styles.bodyImage} />
                                        <Text style={styles.label}>شخص ثالث</Text>
                                    </TouchableOpacity>
                                </View>
                                <Image source={badge} style={{position: 'absolute', right: -3, top: -2, zIndex: 30}} />
                            </View>
                            <View style={styles.row}>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={() => Actions.otherVacleInfo({earthquake: true, pageTitle: 'بیمه زلزله'})} style={styles.imageContainer}>
                                        <Image source={bimeh_zelzele} style={styles.bodyyImage} />
                                        <Text style={styles.label}>زلزله</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={() => Actions.otherVacleInfo({fire: true, pageTitle: 'بیمه آتش سوزی'})} style={styles.imageContainer}>
                                        <Image source={bimeh_atashsoozi} style={styles.bodyyImage} />
                                        <Text style={styles.label}>آتش سوزی</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={() => Actions.otherVacleInfo({age: true, pageTitle: 'بیمه عمر'})} style={styles.imageContainer}>
                                        <Image source={bimeh_omr} style={[styles.bodyImage, {marginBottom: 5, marginTop: 5}]} />
                                        <Text style={styles.label}>عمر</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={() => Actions.otherVacleInfo({individual: true, pageTitle: 'بیمه درمانی (تکمیلی)'})} style={styles.imageContainer}>
                                        <Image source={bimeh_darman} style={styles.bodyyImage} />
                                        <Text style={styles.label}>درمانی(تکمیلی)</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={() => Actions.otherVacleInfo({medical: true, pageTitle: 'بیمه مسئولیت پزشکی'})} style={styles.imageContainer}>
                                        <Image source={bimeh_masooliat} style={styles.bodyyImage} />
                                        <Text style={styles.label}>مسئولیت</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={() => Actions.otherVacleInfo({travel: true, pageTitle: 'بیمه مسافرتی'})} style={styles.imageContainer}>
                                        <Image source={bimeh_mosaferat} style={styles.bodyyImage} />
                                        <Text style={styles.label}>مسافرت</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={styles.lastrow}>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={() => Actions.otherVacleInfo({tamin: true, pageTitle: 'بیمه تامین اجتماعی'})} style={styles.imageContainer}>
                                        <Image source={bimeh_tamin} style={styles.bodyyImage} />
                                        <Text style={styles.label}>تامین اجتماعی</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <View style={styles.reminder}>
                    <Icon name="bell-o" size={25} color="white" />
                    <Text style={styles.reminderText}>یادآور بیمه</Text>
                </View>
                <FooterMenu active="home" />
            </View>
        );
    }
}
export default Home;

