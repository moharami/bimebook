
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput, ImageBackground,  Dimensions} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bani.azarinpro.info/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import HomeHeader from '../../components/homeHeader'
import FooterMenu from '../../components/footerMenu'
import logo from '../../assets/status-icon-01.png'

class BuyMassage extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,

        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

    }
    onDateChange(date) {
        setTimeout(() => {this.setState({showPicker: false})}, 200)
        this.setState({ selectedStartDate: date });

    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        } else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.topHeader}>
                            <HomeHeader notification={false} />
                        </View>
                    </View>
                    {/*<ScrollView style={styles.scroll}>*/}
                    <View style={styles.bodyContainer}>
                        <Image source={logo} style={styles.image}/>
                        <Text style={styles.label}>بیمه نامه شما در حال صدور است</Text>
                        <Text style={styles.label}>به زودی از بیمه بوک با شما تماس گرفته خواهد شد</Text>
                        <TouchableOpacity onPress={()=> Actions.home()} style={styles.reminder}>
                            <Text style={styles.reminderText}>مشاهده بیمه نامه های شما</Text>
                        </TouchableOpacity>
                    </View>
                    {/*</ScrollView>*/}
                    <FooterMenu active="home" />
                </View>
            );
    }
}
export default BuyMassage;

