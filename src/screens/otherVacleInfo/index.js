
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, Picker, AsyncStorage, BackHandler, Alert, TextInput,  Dimensions} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bani.azarinpro.info/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import HomeHeader from '../../components/homeHeader'
import FooterMenu from '../../components/footerMenu'
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

import Icon from 'react-native-vector-icons/dist/FontAwesome';
import moment_jalaali from 'moment-jalaali'
import PersianCalendarPicker from 'react-native-persian-calendar-picker';
import Slider from "react-native-slider";
import SwitchRow from '../../components/switchRow';

class OtherVacleInfo extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            input1: 0,
            input1Title:null,
            input2: 0,
            input2Title:null,
            input3: 0,
            input3Title:null,
            input4: 0,
            input4Title:null,
            inputTxt1: '',
            inputTxt2: '',
            inputTxt3: '',
            earthquake: false,
            pipe: false,
            earth: false,
            rain: false,
            tornado: false,
            flood: false,
            airplane: false,
            steal: false,
            area_price: 1,
            sliderChange: false,
            countries: [],
            age: 7,
            ageTitle:null,
            expert: [],
            feature: []
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

    }
    onDateChange(date) {
        setTimeout(() => {this.setState({showPicker: false})}, 200)
        this.setState({ selectedStartDate: date });

    }
    switchButtons(status, id, title) {
        console.log(status)
        if(id === 1) {
            this.setState({
                earthquake: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 2) {
            this.setState({
                pipe: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 3) {
            this.setState({
                earth: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 4) {
            this.setState({
                rain: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 5) {
            this.setState({
                tornado: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 6) {
            this.setState({
                flood: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 7) {
            this.setState({
                airplane: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 8) {
            this.setState({
                steal: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        } else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.topHeader}>
                            <HomeHeader />
                        </View>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            <View style={styles.headerContainer}>
                                <Text style={styles.headerText}>{this.props.pageTitle}</Text>
                            </View>
                            {
                                this.props.age ?
                                    <View style={styles.itemContainer}>
                                        <View style={styles.pickerContainer}>
                                            <View style={styles.innerPickerContainer}>
                                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, left: 10}}/>
                                                    <Picker
                                                        mode="dropdown"
                                                        style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                        selectedValue={this.state.input1}
                                                        // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                        onValueChange={(itemValue) =>{
                                                            this.setState({
                                                                input1: itemValue
                                                            })}}>
                                                        <Picker.Item label="حق بیمه" value={0} />
                                                        <Picker.Item  label='sdew' value={1} />
                                                    </Picker>
                                                </View>
                                            </View>
                                            <View style={styles.innerPickerContainer}>
                                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, left: 10}}/>
                                                    <Picker
                                                        mode="dropdown"
                                                        style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                        selectedValue={this.state.input2}
                                                        // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                        onValueChange={(itemValue) =>{
                                                            this.setState({
                                                                input2: itemValue
                                                            })}}>
                                                        <Picker.Item label="نحوه پرداخت" value={0} />
                                                        <Picker.Item  label='sdew' value={1} />
                                                    </Picker>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={[styles.pickerContainer, {justifyContent: 'flex-end'}]}>
                                            <View style={[styles.innerPickerContainer, {alignSelf:'flex-end'}]}>
                                                <View style={{position: 'relative', zIndex: 3, width: '94%', height: 30}}>
                                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, right: 8}}/>
                                                    <Picker
                                                        mode="dropdown"
                                                        style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                        selectedValue={this.state.input3}
                                                        // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                        onValueChange={(itemValue) =>{
                                                            this.setState({
                                                                input3: itemValue
                                                            })}}>
                                                        <Picker.Item label="طول مدت قرارداد" value={0} />
                                                        <Picker.Item  label='sdew' value={1} />
                                                    </Picker>
                                                </View>
                                            </View>
                                        </View>
                                    </View>

                                    : null
                            }
                            {
                                this.props.fire ?
                                    <View style={styles.itemContainer}>
                                        <View style={styles.pickerContainer}>
                                            <View style={styles.innerPickerContainer}>
                                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, left: 10}}/>
                                                    <Picker
                                                        mode="dropdown"
                                                        style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                        selectedValue={this.state.input2}
                                                        // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                        onValueChange={(itemValue) =>{
                                                            this.setState({
                                                                input2: itemValue
                                                            })}}>
                                                        <Picker.Item label="نوع سازه" value={0} />
                                                        <Picker.Item  label='sdew' value={1} />
                                                    </Picker>
                                                </View>
                                            </View>
                                            <View style={styles.innerPickerContainer}>
                                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, left: 10}}/>
                                                    <Picker
                                                        mode="dropdown"
                                                        style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                        selectedValue={this.state.input1}
                                                        // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                        onValueChange={(itemValue) =>{
                                                            this.setState({
                                                                input1: itemValue
                                                            })}}>
                                                        <Picker.Item label="نوع ملک" value={0} />
                                                        <Picker.Item  label='sdew' value={1} />
                                                    </Picker>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={styles.pickerContainer}>
                                            <View style={[styles.innerPickerContainer, {alignSelf:'flex-end'}]}>
                                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                    <TextInput
                                                        placeholder='ارزش لوازم خانگی'
                                                        placeholderTextColor={'gray'}
                                                        underlineColorAndroid='transparent'
                                                        value={this.state.inputTxt1}
                                                        style={{
                                                            height: 36,
                                                            marginBottom: 10,
                                                            backgroundColor: 'white',
                                                            paddingRight: 25,
                                                            width: '100%',
                                                            color: 'gray',
                                                            fontSize: 14,
                                                            textAlign: 'right',
                                                            // elevation: 4,
                                                            shadowColor: 'lightgray',
                                                            shadowOffset: {width: 5,height: 5},
                                                            shadowOpacity: 1 ,
                                                        }}
                                                        onChangeText={(text) =>this.setState({inputTxt1: text})}
                                                    />
                                                </View>
                                            </View>
                                            <View style={[styles.innerPickerContainer, {alignSelf:'flex-end'}]}>
                                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                    <TextInput
                                                        placeholder='تعداد واحد'
                                                        placeholderTextColor={'gray'}
                                                        underlineColorAndroid='transparent'
                                                        value={this.state.inputTxt2}
                                                        style={{
                                                            height: 36,
                                                            marginBottom: 10,
                                                            backgroundColor: 'white',
                                                            paddingRight: 25,
                                                            width: '100%',
                                                            color: 'gray',
                                                            fontSize: 14,
                                                            textAlign: 'right',
                                                            // elevation: 4,

                                                        }}
                                                        onChangeText={(text) =>this.setState({inputTxt2: text})}
                                                    />
                                                </View>
                                            </View>
                                        </View>
                                        <View style={[styles.pickerContainer, {justifyContent: 'flex-end', marginBottom: 15}]}>
                                            <View style={[styles.innerPickerContainer, {alignSelf:'flex-end'}]}>
                                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                    <TextInput
                                                        placeholder='متراژ مورد بیمه'
                                                        placeholderTextColor={'gray'}
                                                        underlineColorAndroid='transparent'
                                                        value={this.state.inputTxt3}
                                                        style={{
                                                            height: 36,
                                                            marginBottom: 10,
                                                            backgroundColor: 'white',
                                                            paddingRight: 25,
                                                            width: '100%',
                                                            color: 'gray',
                                                            fontSize: 14,
                                                            textAlign: 'right',

                                                        }}
                                                        onChangeText={(text) =>this.setState({inputTxt3: text})}
                                                    />
                                                </View>
                                            </View>
                                        </View>
                                        <Text style={[styles.txt, {alignSelf: 'flex-end'}]}>هزینه ساخت هر متر مربع</Text>
                                        <Slider
                                            style={{
                                                flex: 1,
                                                width: '90%',
                                                marginLeft: 10,
                                                marginTop: 20,
                                                marginRight: 10,
                                                alignItems: "stretch",
                                                justifyContent: "center",

                                                // backgroundColor: '#00b3bc'
                                            }}
                                            minimumValue={1}
                                            maximumValue={5}
                                            maximumTrackTintColor="#00b3bc"
                                            // minimumTrackTintColor="red"
                                            step={.5}
                                            value={this.state.area_price}
                                            onValueChange={value => this.setState({area_price: value, sliderChange: true})}/>
                                        <Text style={{textAlign: 'center'}}>{this.state.area_price}</Text>
                                        <View style={{width: '90%'}}>
                                            <SwitchRow label="زلزله"  switchButtons={(status) => this.switchButtons(status, 1, 'earthquake')} />
                                            <SwitchRow label="ترکیدگی لوله"  switchButtons={(status) => this.switchButtons(status, 2, 'pipe')} />
                                            <SwitchRow label="نشست زمین"  switchButtons={(status) => this.switchButtons(status, 3, 'earth')} />
                                            <SwitchRow label="ضایعات ناشی از برف و باران"  switchButtons={(status) => this.switchButtons(status, 4, 'rain')} />
                                            <SwitchRow label="طوفان"  switchButtons={(status) => this.switchButtons(status, 5, 'tornado')} />
                                            <SwitchRow label="سیل"  switchButtons={(status) => this.switchButtons(status, 6, 'flood')} />
                                            <SwitchRow label="سقوط هواپیما"  switchButtons={(status) => this.switchButtons(status, 7, 'airplane')} />
                                            <SwitchRow label="سرقت مربوط به شکست حرز"  switchButtons={(status) => this.switchButtons(status, 8, 'steal')} />
                                        </View>
                                    </View>

                                    : null
                            }
                            {
                                this.props.medical ?
                                    <View style={styles.itemContainer}>
                                        <View style={styles.pickerContainer}>
                                            <View style={styles.innerPickerContainer}>
                                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, left: 10}}/>
                                                    <Picker
                                                        mode="dropdown"
                                                        style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                        selectedValue={this.state.input1}
                                                        // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                        onValueChange={(itemValue) =>{
                                                            this.setState({
                                                                input1: itemValue
                                                            })}}>
                                                        <Picker.Item label="نوع تخصص" value={0} />
                                                        <Picker.Item  label='sdew' value={1} />
                                                    </Picker>
                                                </View>
                                            </View>
                                            <View style={styles.innerPickerContainer}>
                                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, left: 10}}/>
                                                    <Picker
                                                        mode="dropdown"
                                                        style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                        selectedValue={this.state.input2}
                                                        // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                        onValueChange={(itemValue) =>{
                                                            this.setState({
                                                                input2: itemValue
                                                            })}}>
                                                        <Picker.Item label="نوع بیمه" value={0} />
                                                        <Picker.Item  label='sdew' value={1} />
                                                    </Picker>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={styles.pickerContainer}>
                                            <View style={styles.innerPickerContainer}>
                                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, left: 10}}/>
                                                    <Picker
                                                        mode="dropdown"
                                                        style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                        selectedValue={this.state.input3}
                                                        // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                        onValueChange={(itemValue) =>{
                                                            this.setState({
                                                                input3: itemValue
                                                            })}}>
                                                        <Picker.Item label="تعداد دیه" value={0} />
                                                        <Picker.Item  label='sdew' value={1} />
                                                    </Picker>
                                                </View>
                                            </View>
                                            <View style={styles.innerPickerContainer}>
                                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, left: 10}}/>
                                                    <Picker
                                                        mode="dropdown"
                                                        style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                        selectedValue={this.state.input4}
                                                        // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                        onValueChange={(itemValue) =>{
                                                            this.setState({
                                                                input4: itemValue
                                                            })}}>
                                                        <Picker.Item label="مدت بیمه نامه" value={0} />
                                                        <Picker.Item  label='sdew' value={1} />
                                                    </Picker>
                                                </View>
                                            </View>
                                        </View>
                                    </View>

                                    : null
                            }
                            {
                                this.props.individual ?
                                    <View style={styles.itemContainer}>
                                        <View style={styles.pickerContainer}>
                                            <View style={styles.innerPickerContainer}>
                                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, left: 10}}/>
                                                    <Picker
                                                        mode="dropdown"
                                                        style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                        selectedValue={this.state.input4}
                                                        // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                        onValueChange={(itemValue) =>{
                                                            this.setState({
                                                                input4: itemValue
                                                            })}}>
                                                        <Picker.Item label="بیمه گر پایه" value={0} />
                                                        <Picker.Item  label='sdew' value={1} />
                                                    </Picker>
                                                </View>
                                            </View>
                                            <View style={styles.innerPickerContainer}>
                                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, left: 10}}/>
                                                    <Picker
                                                        mode="dropdown"
                                                        style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                        selectedValue={this.state.input1}
                                                        // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                        onValueChange={(itemValue) =>{
                                                            this.setState({
                                                                input1: itemValue
                                                            })}}>
                                                        <Picker.Item label="نوع بیمه" value={0} />
                                                        <Picker.Item label="انفرادی" value={2} />
                                                        <Picker.Item label="گروهی" value={1} />
                                                    </Picker>
                                                </View>
                                            </View>
                                        </View>
                                        {
                                            this.state.input1 === 2 ?
                                                <View style={styles.pickerContainer}>
                                                    <View style={styles.innerPickerContainer}>
                                                        <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, left: 10}}/>
                                                            <Picker
                                                                mode="dropdown"
                                                                style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                                selectedValue={this.state.age}
                                                                // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                                onValueChange={(itemValue) =>{
                                                                    this.setState({
                                                                        age: itemValue
                                                                    })}}>
                                                                <Picker.Item label="سن" value={0} />
                                                                <Picker.Item  label='sdew' value={1} />
                                                            </Picker>
                                                        </View>
                                                    </View>
                                                    <View style={styles.innerPickerContainer}>
                                                        <View style={{position: 'relative', zIndex: 3, width: '94%', height: 30}}>
                                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, right: 10}}/>
                                                            <Picker
                                                                mode="dropdown"
                                                                style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                                selectedValue={this.state.input2}
                                                                // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                                onValueChange={(itemValue) =>{
                                                                    this.setState({
                                                                        input2: itemValue
                                                                    })}}>
                                                                <Picker.Item label="پوشش دندان پزشکی" value={0} />
                                                                <Picker.Item  label='sdew' value={1} />
                                                            </Picker>
                                                        </View>
                                                    </View>
                                                </View>
                                                : null
                                        }
                                        {
                                            this.state.input1 === 1 ?
                                                <View style={[styles.pickerContainer,  {justifyContent:'flex-end'}]}>
                                                    <View style={styles.innerPickerContainer}>
                                                        <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                            <TextInput
                                                                placeholder='تعداد نفرات'
                                                                placeholderTextColor={'gray'}
                                                                underlineColorAndroid='transparent'
                                                                value={this.state.inputTxt1}
                                                                style={{
                                                                    height: 36,
                                                                    marginBottom: 10,
                                                                    backgroundColor: 'white',
                                                                    paddingRight: 25,
                                                                    width: '100%',
                                                                    color: 'gray',
                                                                    fontSize: 14,
                                                                    textAlign: 'right',
                                                                    // elevation: 4,
                                                                    shadowColor: 'lightgray',
                                                                    shadowOffset: {width: 5,height: 5},
                                                                    shadowOpacity: 1 ,
                                                                }}
                                                                onChangeText={(text) =>this.setState({inputTxt1: text})}
                                                            />
                                                        </View>
                                                    </View>
                                                </View>
                                                 : null
                                        }
                                    </View>

                                    : null
                            }
                            {
                                this.props.travel ?
                                    <View style={styles.itemContainer}>
                                        <View style={styles.pickerContainer}>
                                            <View style={styles.innerPickerContainer}>
                                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, left: 10}}/>
                                                    <Picker
                                                        mode="dropdown"
                                                        style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                        selectedValue={this.state.input1}
                                                        // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                        onValueChange={(itemValue) =>{
                                                            this.setState({
                                                                input1: itemValue
                                                            })}}>
                                                        <Picker.Item label="کشور" value={0} />
                                                        <Picker.Item  label='sdew' value={1} />
                                                    </Picker>
                                                </View>
                                            </View>
                                            <View style={styles.innerPickerContainer}>
                                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, left: 10}}/>
                                                    <Picker
                                                        mode="dropdown"
                                                        style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                        selectedValue={this.state.input2}
                                                        // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                        onValueChange={(itemValue) =>{
                                                            this.setState({
                                                                input2: itemValue
                                                            })}}>
                                                        <Picker.Item label="نوع بیمه" value={0} />
                                                        <Picker.Item  label='sdew' value={1} />
                                                    </Picker>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={styles.pickerContainer}>
                                            <View style={styles.innerPickerContainer}>
                                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, left: 10}}/>
                                                    <Picker
                                                        mode="dropdown"
                                                        style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                        selectedValue={this.state.input3}
                                                        // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                        onValueChange={(itemValue) =>{
                                                            this.setState({
                                                                input3: itemValue
                                                            })}}>
                                                        <Picker.Item label="مدت اقامت" value={0} />
                                                        <Picker.Item  label='sdew' value={1} />
                                                    </Picker>
                                                </View>
                                            </View>
                                            <View style={styles.innerPickerContainer}>
                                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, left: 10}}/>
                                                    <Picker
                                                        mode="dropdown"
                                                        style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                        selectedValue={this.state.age}
                                                        // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                        onValueChange={(itemValue) =>{
                                                            this.setState({
                                                                age: itemValue
                                                            })}}>
                                                        <Picker.Item label="سن" value={0} />
                                                        <Picker.Item  label='sdew' value={1} />
                                                    </Picker>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                    : null
                            }
                            <View style={styles.itemContainer}>
                                <TouchableOpacity onPress={() => Actions.results()} style={styles.advertise}>
                                    <Text style={styles.buttonTitle}>مقایسه کن!</Text>
                                </TouchableOpacity>
                            </View>
                            {
                                this.state.showPicker ?
                                    <View style={{
                                        position: 'absolute',
                                        bottom: '50%',
                                        zIndex: 9999,
                                        backgroundColor: 'white'
                                    }}>
                                        <PersianCalendarPicker
                                            onDateChange={(date) => this.onDateChange(date)}
                                        />
                                    </View>
                                    : null
                            }
                        </View>
                    </ScrollView>
                    <View style={styles.reminder}>
                        <Icon name="bell-o" size={25} color="white" />
                        <Text style={styles.reminderText}>یادآور بیمه</Text>
                    </View>
                    <FooterMenu active="home" />
                </View>
            );
    }
}
export default OtherVacleInfo;

