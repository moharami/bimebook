

import { StyleSheet, Dimensions} from 'react-native';
// import env from '../../colors/env';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(246, 246, 246)',
        // backgroundColor: 'red'
    },
    header: {
        flex: 1,
        height: '22%',
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9990
    },
    topHeader: {
        backgroundColor: 'rgb(20, 85, 151)',
        width: '100%',
        height: '45%'
    },
    bodyContainer: {
        width: '100%',
        paddingBottom: 180,
        paddingTop: 20,
        padding: 10,
        backgroundColor: 'rgb(248, 248, 248)',
        alignItems: 'center',
        justifyContent: 'center',

    },
    insuranceInfoContainer: {
        width: '100%',
        padding: 10,
        backgroundColor: 'rgb(168, 170, 173)',
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    labelContainer: {
        flexDirection: 'row',
        paddingRight: 10

    },
    imageContainer: {
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        padding: 10
    },
    rightContainer: {
        // flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 3,
        paddingLeft: 3,
        height: 27,
        // borderRadius: 20,
        backgroundColor: 'rgb(11, 81, 144)',
        // overflow: 'hidden'
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderTopLeftRadius: 10,
        marginRight: 5
        // overflow: 'hidden',

    },
    contentText: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'white',
        paddingRight: 10
    },
    label: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'white'
    },
    insurancerContainer: {
        width: '100%',
        padding: 6,
        backgroundColor: 'white',
        borderRadius: 10
    },
    insuranceRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    title: {
        fontFamily: 'IRANSansMobile(FaNum)_Bold',
        fontSize: 18,
        color: 'white'
    },
    bodyLabel: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'black',
        alignSelf: 'flex-end',
        paddingTop: 15,
        paddingBottom: 5
    },
    scroll: {
        paddingTop: 45,
        backgroundColor: 'rgb(249, 249, 249)'
    },
    reminder: {
        position: 'absolute',
        right: '30%',
        bottom: 85,
        zIndex: 40,
        alignItems: 'center',
        justifyContent: 'center',

        width: '40%',
        borderRadius: 30,
        backgroundColor: '#8dc63f',
        padding: 11,
        marginTop: 35
    },
    reminderText: {
        fontSize: 13,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    headerContainer: {
        alignItems: 'flex-end',
        justifyContent: 'center',
        padding: 15,
        borderBottomWidth: 1,
        borderBottomColor: 'rgb(237, 237, 237)',
        backgroundColor: 'white'
    },
    headerText: {
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black',
        alignSelf: 'flex-end'
    },
    pickerContainer: {
        width: '90%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    advertise: {
        width: '40%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#8dc63f',
        padding: 8,
        marginTop: 35
    },
    buttonTitle: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    insuranceContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 20
    },
    mapText: {
        fontSize: 14,
        color: 'rgb(11, 81, 144)',
        fontFamily: 'IRANSansMobile(FaNum)',
        textDecorationLine: 'underline'
    },
    mapStyle: {
        width: '100%',
        height: Dimensions.get('screen').height
    }
});

