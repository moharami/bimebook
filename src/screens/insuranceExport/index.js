
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput, ImageBackground,Animated,  Dimensions} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bani.azarinpro.info/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import PageHeader from '../../components/pageHeader'
import FooterMenu from '../../components/footerMenu'
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import asia from '../../assets/asia.png'
import InfoItem from "../../components/infoItem/index";

class InsuranceExport extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        } else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.topHeader}>
                            <PageHeader carInfo={false} title="درخواست صدور" />
                        </View>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            <Text style={styles.bodyLabel}>مشخصات بیمه نامه</Text>
                            <View style={styles.insuranceInfoContainer}>
                                <View>
                                    <View style={styles.labelContainer}>
                                        <View style={styles.rightContainer}>
                                            <Text style={styles.label}>بیمه شخص ثالث</Text>
                                        </View>
                                        <Text style={styles.title}>بیمه آسیا</Text>
                                    </View>
                                    <Text style={[styles.contentText, {paddingBottom: 8, paddingTop: 4}]}>تاریخ صدور بیمه نامه: 97/6/7</Text>
                                    <Text style={styles.contentText}>مبلغ نهایی: 1,250,258 تومان</Text>
                                </View>
                                <View style={styles.imageContainer}>
                                    <Image source={asia} style={{width: 65, height: 65, resizeMode: 'contain'}} />
                                </View>
                            </View>
                            <Text style={styles.bodyLabel}>مشخصات بیمه گذار</Text>
                            <View style={styles.insurancerContainer}>
                                <View style={[styles.insuranceRow, {marginBottom: 6}]}>
                                    <InfoItem label="کد ملی" value="" />
                                    <InfoItem label="نام" value="" />
                                </View>
                                <View style={[styles.insuranceRow, {marginBottom: 6}]}>
                                    <InfoItem label="پلاک" value="" />
                                    <InfoItem label="خودرو" value="" />
                                </View>
                                <View style={styles.insuranceRow}>
                                    <InfoItem label="شماره بدنه" value="" />
                                    <InfoItem label="شماره شاسی" value="" />
                                </View>
                            </View>
                            <Text style={styles.bodyLabel}>مشخصات تحویل گیرنده</Text>
                            <View style={styles.insurancerContainer}>
                                <View style={[styles.insuranceRow, {marginBottom: 6}]}>
                                    <InfoItem label="شماره موبایل" value="" />
                                    <InfoItem label="نام تحویل گیرنده" value="" />
                                </View>
                                <View style={styles.insuranceRow}>
                                    <InfoItem label="نشانی:" value="" long />
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                    <TouchableOpacity onPress={()=> Actions.home()} style={styles.reminder}>
                        <Text style={styles.reminderText}>ثبت سفارش</Text>
                    </TouchableOpacity>
                    <FooterMenu active="home" />
                </View>
            );
    }
}
export default InsuranceExport;
