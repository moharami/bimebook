
import { StyleSheet, Dimensions} from 'react-native';
// import env from '../../colors/env';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(246, 246, 246)',
        // backgroundColor: 'red'
    },
    header: {
        flex: 1,
        height: '22%',
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9990
    },
    topHeader: {
        backgroundColor: 'rgb(20, 85, 151)',
        width: '100%',
        height: '45%'
    },
    headerTitle: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: '19%'
    },
    bodyContainer: {

        width: '100%',
        height: Dimensions.get('screen').height ,
        // paddingBottom: 180,
        // paddingTop: 20,
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    scroll: {
        paddingTop: 45,
        // backgroundColor: 'white'
    },
    image: {
        width: '80%',
        height: Dimensions.get('screen').height*.2,
        resizeMode: 'contain',
        marginTop: '30%'
    },
    reminder: {
        // position: 'absolute',
        // bottom: 85,
        // zIndex: 40,
        alignItems: 'center',
        justifyContent: 'center',

        width: '40%',
        borderRadius: 30,
        backgroundColor: '#8dc63f',
        padding: 11,
        marginTop: 35
    },
    reminderText: {
        fontSize: 13,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    label: {
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black',
        paddingTop: 15
    }

});


