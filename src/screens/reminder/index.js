
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput, ImageBackground,  Dimensions} from 'react-native';
import styles from './styles'
import tag from '../../assets/tag.png'
import {Actions} from 'react-native-router-flux'
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bani.azarinpro.info/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import PageHeader from '../../components/pageHeader'
import FooterMenu from '../../components/footerMenu'
import ReminderItem from '../../components/reminderItem'
import Icon from 'react-native-vector-icons/dist/FontAwesome';

class Reminder extends Component {

    constructor(props){
        super(props);
        this.state = {
            loading: false,
            active: 1
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        } else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.topHeader}>
                            <PageHeader carInfo={false} title="یادآور بیمه" />
                        </View>
                        <ScrollView style={{ transform: [
                            { scaleX: -1}
                        ], width: '100%'}}
                                    horizontal={true} showsHorizontalScrollIndicator={false}
                        >
                            <View style={styles.catContainer}>
                                <TouchableOpacity onPress={() => this.setState({active: 3})}>
                                    <View style={[styles.navContainer, {borderBottomWidth: this.state.active === 3 ? 4 : 0, borderBottomColor: '#8dc63f'}]}>
                                        <Text style={styles.text}>ساختمان</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.setState({active: 2})}>
                                    <View style={[styles.navContainer, {borderBottomWidth: this.state.active === 2 ? 4 : 0, borderBottomColor: '#8dc63f'}]}>
                                        <Text style={styles.text}>خودرو</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.setState({active: 1})}>
                                    <View style={[styles.navContainer, {borderBottomWidth: this.state.active === 1 ? 4 : 0, borderBottomColor: '#8dc63f'}]}>
                                        <Text style={styles.text}>همه موارد</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            <ReminderItem />
                            <ReminderItem />
                            <TouchableOpacity onPress={()=> Actions.home()} style={styles.reminder}>
                                <Text style={styles.reminderText}>اضافه کردن سررسید بیمه جدید</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                    <FooterMenu active="home" />
                </View>
            );
    }
}
export default Reminder;

