
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput, ImageBackground,  Dimensions} from 'react-native';
import styles from './styles'
import tag from '../../assets/tag.png'
import {Actions} from 'react-native-router-flux'
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bani.azarinpro.info/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import PageHeader from '../../components/pageHeader'
import FooterMenu from '../../components/footerMenu'
import UserInsurancesItem from '../../components/userInsuranceItem'
import TransactionBox from '../../components/transactionBox'
import TransactionItem from '../../components/transactionItem'
import Icon from 'react-native-vector-icons/dist/FontAwesome';

class UserInsurances extends Component {

    constructor(props){
        super(props);
        this.state = {
            loading: false,
            active: 1
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        } else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.topHeader}>
                            <PageHeader carInfo={false} title="بیمه نامه های من" />
                        </View>
                        <ScrollView style={{ transform: [
                            { scaleX: -1}
                        ], width: '100%'}}
                                    horizontal={true} showsHorizontalScrollIndicator={false}
                        >
                            <View style={styles.catContainer}>
                                <TouchableOpacity onPress={() => this.setState({active: 3})}>
                                    <View style={[styles.navContainer, {borderBottomWidth: this.state.active === 3 ? 4 : 0, borderBottomColor: '#8dc63f'}]}>
                                        <Text style={styles.text}>بدنه</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.setState({active: 2})}>
                                    <View style={[styles.navContainer, {borderBottomWidth: this.state.active === 2 ? 4 : 0, borderBottomColor: '#8dc63f'}]}>
                                        <Text style={styles.text}>شخص ثالث</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.setState({active: 1})}>
                                    <View style={[styles.navContainer, {borderBottomWidth: this.state.active === 1 ? 4 : 0, borderBottomColor: '#8dc63f'}]}>
                                        <Text style={styles.text}>همه موارد</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            <UserInsurancesItem />
                            <UserInsurancesItem />
                            <Text style={styles.transactionTxt}>تراکنش ها</Text>
                            <View style={styles.transactions}>
                                <TransactionBox title="مبلغ اقساط باقی مانده" value="1,200,000 تومان" />
                                <TransactionBox title="پرداخت نقدی" value="1,450,000 تومان" />
                            </View>
                            {/*<TouchableOpacity onPress={()=> Actions.home()} style={styles.reminder}>*/}
                                {/*<Text style={styles.reminderText}>اضافه کردن سررسید بیمه جدید</Text>*/}
                            {/*</TouchableOpacity>*/}
                        </View>
                    </ScrollView>
                    <FooterMenu active="home" />
                </View>
            );
    }
}
export default UserInsurances;

