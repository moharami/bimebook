
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput, ImageBackground,  Dimensions} from 'react-native';
import styles from './styles'
import tag from '../../assets/tag.png'
import {Actions} from 'react-native-router-flux'
import FIcon from 'react-native-vector-icons/dist/Feather';
import OIcon from 'react-native-vector-icons/dist/Octicons';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bani.azarinpro.info/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import PageHeader from '../../components/pageHeader'
import FooterMenu from '../../components/footerMenu'
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import ImagePicker from 'react-native-image-picker'

class UploadDocs extends Component {

    constructor(props){
        super(props);
        this.state = {
            loading: false,
            input1: '',
            input2: '',
            input3: '',
            shasiNumber: '',
            bodyNumber: '',
            src1: null,
            src2: null,
            src3: null
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

    }
    selectPhotoTapped(id) {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                if(id == 1){
                    this.setState({
                        src1: response.uri
                    })
                }
                else if(id === 2){
                    this.setState({
                        src2: response.uri
                    })
                }
                else if (id === 3){
                    this.setState({
                        src3: response.uri
                    })
                }
            }
        });
    }
    render() {
        console.log(this.state.src1)
        console.log(this.state.src2)
        console.log(this.state.src3)
        if(this.state.loading){
            return (<Loader />)
        } else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.topHeader}>
                            <PageHeader carInfo={false} title="ارسال مدارک" />
                        </View>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            <Text style={styles.label}>لطفا تصویر مدارک مورد نیاز زیر را ارسال نمایید:</Text>
                            {
                                this.state.src1 ?  <Text style={styles.inputLabel}>تصویر بیمه نامه قبلی</Text> : null
                            }
                            <View style={{position: 'relative', zIndex: 1, width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                <TouchableOpacity onPress={() => this.selectPhotoTapped(1)} style={{position: 'absolute', zIndex: 90, top: '22%', left: '15%'}}>
                                    <FIcon name="camera" size={20} color="rgb(11, 81, 144)" />
                                </TouchableOpacity>
                                <TextInput
                                    placeholderTextColor={'gray'}
                                    underlineColorAndroid='transparent'
                                    placeholder={this.state.src1 !== null ? "image.jpg" : "تصویر بیمه نامه قبلی"}
                                    value={this.state.src1 !== null ? "image.jpg" : "تصویر بیمه نامه قبلی"}
                                    style={{
                                        textAlign: 'right',
                                        borderWidth: 1,
                                        borderColor: 'rgb(207, 207, 207)',
                                        height: 50,
                                        backgroundColor: this.state.src1 !== null ? "white" : "transparent",
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        width: '80%',
                                        borderRadius: 25,
                                        fontSize: 14,
                                        color: '#7A8299',
                                        marginBottom: 20
                                    }}
                                    onChangeText={(text) => {
                                        this.setState({shasiNumber: text})

                                    }}/>
                            </View>

                            {
                                this.state.src2 ?  <Text style={styles.inputLabel}>تصویر کارت خودرو</Text> : null

                            }
                            <View style={{position: 'relative', zIndex: 1, width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                <TouchableOpacity onPress={() => this.selectPhotoTapped(2)} style={{position: 'absolute', zIndex: 90, top: '22%', left: '15%'}}>
                                    <FIcon name="camera" size={20} color="rgb(11, 81, 144)" />
                                </TouchableOpacity>
                                <TextInput
                                    placeholderTextColor={'gray'}
                                    placeholder={this.state.src2 !== null ? "image.jpg" : "تصویر کارت خودرو"}
                                    value={this.state.src2 !== null ? "image.jpg" : "تصویر کارت خودرو"}
                                    underlineColorAndroid='transparent'
                                    style={{
                                        textAlign: 'right',
                                        borderWidth: 1,
                                        borderColor: 'rgb(207, 207, 207)',
                                        height: 50,
                                        backgroundColor: this.state.src2 !== null ? "white" : "transparent",
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        width: '80%',
                                        borderRadius: 25,
                                        fontSize: 14,
                                        color: '#7A8299',
                                        borderStyle: 'dashed',
                                        marginBottom: 20

                                    }}
                                    onChangeText={(text) => {
                                        this.setState({bodyNumber: text})

                                    }}/>
                            </View>
                            {
                                this.state.src3 ?  <Text style={styles.inputLabel}>تصویر کارت ملی</Text> : null
                            }
                            <View style={{position: 'relative', zIndex: 1, width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                <TouchableOpacity onPress={() => this.selectPhotoTapped(3)} style={{position: 'absolute', zIndex: 90, top: '22%', left: '15%'}}>
                                    <FIcon name="camera" size={20} color="rgb(11, 81, 144)" />
                                </TouchableOpacity>
                                <TextInput
                                    placeholderTextColor={'gray'}
                                    placeholder={this.state.src3 !== null ? "image.jpg" : "تصویر کارت ملی"}
                                    underlineColorAndroid='transparent'
                                    value={this.state.src3 !== null ? "image.jpg" : "تصویر کارت ملی"}
                                    style={{
                                        textAlign: 'right',
                                        borderWidth: 1,
                                        borderColor: 'rgb(207, 207, 207)',
                                        height: 50,
                                        backgroundColor: this.state.src3 !== null ? "white" : "transparent",
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        width: '80%',
                                        borderRadius: 25,
                                        fontSize: 14,
                                        color: '#7A8299',
                                        borderStyle: 'dashed'
                                    }}
                                    onChangeText={(text) => {
                                        this.setState({bodyNumber: text})
                                    }}/>
                            </View>
                            <TouchableOpacity onPress={() => Actions.home()} style={styles.advertise}>
                                <Text style={styles.buttonTitle}>ارسال</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                    <View style={styles.reminder}>
                        <Icon name="bell-o" size={25} color="white" />
                        <Text style={styles.reminderText}>یادآور بیمه</Text>
                    </View>
                    <FooterMenu active="home" />
                </View>
            );
    }
}
export default UploadDocs;

