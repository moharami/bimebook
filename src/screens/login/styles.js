import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
import {Dimensions} from 'react-native'
export default StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        alignItems:'flex-end',
        justifyContent: 'space-between',
        backgroundColor:'#f8f8f8'
    },
    footerImage: {
        width: '80%',
        height: '40%'
    },
    top: {
        width: '100%',
        alignItems:'center',
        justifyContent: 'center',
    },
    contentText: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    logoImage: {
        width: Dimensions.get('window').width*1.5,
        height: 50,
        resizeMode: 'contain',
        marginTop: '20%',
        marginBottom: '10%',
    },
    advertise: {
        width: '30%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#8dc63f',
        padding: 8,
        marginTop: 20

    },
    buttonTitle: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    }
});
