import React, {Component} from 'react';
import {View, TouchableOpacity, Text, TextInput, Image,BackHandler, Alert, AsyncStorage, KeyboardAvoidingView} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux';
// import Axios from 'axios'
// export const url = 'http://bani.azarinpro.info/api/v1';
// Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import footerImage from '../../assets/footer.png'
import logoImage from '../../assets/bimebook-logo.png'

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            loading: false,
            mobile: ''
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh: Math.random()}});
        return true;
    };
    componentWillMount() {
        // AsyncStorage.removeItem('token')
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        }
        else return (
            <View style={styles.container}>
                <View style={styles.top}>
                    <Image source={logoImage} style={styles.logoImage} />
                    <Text style={styles.contentText}>جهت ورود یا ثبت نام شماره موبایل خود را وارد کنید:</Text>
                    <View style={{ flexDirection: 'row', marginTop: 20, borderWidth: 1, borderColor: '#C8C8C8', borderRadius: 20, padding: 0, backgroundColor: 'white'}}>
                        <View style={{
                            borderRadius: 20,
                            backgroundColor: 'white',
                            alignItems: 'center',
                            justifyContent: 'center',
                            height: 45
                        }}>
                            <Icon name={'mobile'}
                                  style={{fontSize: 30, paddingRight: 15, paddingLeft: 15}}/>
                        </View>
                        <TextInput
                            autoFocus={true}
                            // placeholder="شماره موبایل"
                            placeholderTextColor={'#C8C8C8'}
                            underlineColorAndroid='transparent'
                            value={this.state.mobile}
                            style={{
                                borderRadius: 20,
                                textAlign: 'left',
                                height: 45,
                                backgroundColor: 'white',
                                // paddingRight: 15,
                                flex: .8,
                                borderTopLeftRadius: 20,
                                borderBottomLeftRadius: 20,
                                borderBottomRightRadius: 20,
                                borderTopRightRadius: 20,
                                fontSize: 18,
                                color: '#7A8299',
                                fontFamily: 'IRANSansMobile(FaNum)'
                            }}
                            onChangeText={(text) => this.setState({mobile: text})}/>
                    </View>
                    <TouchableOpacity onPress={() => Actions.confirmation()} style={styles.advertise}>
                        <Text style={styles.buttonTitle}>ادامه </Text>
                    </TouchableOpacity>
                </View>
                <Image source={footerImage} style={styles.footerImage} />
            </View>
        );
    }
}

export default Login;