
import { StyleSheet, Dimensions} from 'react-native';
// import env from '../../colors/env';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(246, 246, 246)',
        // backgroundColor: 'red'
    },
    header: {
        flex: 1,
        height: '10%',
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9990,
        backgroundColor: 'rgb(247, 247, 247)'
    },
    topHeader: {
        backgroundColor: 'rgb(20, 85, 151)',
        width: '100%',
        height: '100%'
    },
    headerImage: {
        width: '100%'
    },
    headerTitle: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: '19%'
    },
    bodyContainer: {
        width: '100%',
        paddingBottom: 200,
        backgroundColor: 'rgb(248, 248, 248)',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },
    scroll: {
        // paddingTop: 10,
        backgroundColor: 'rgb(249, 249, 249)',
        paddingTop: '18%',

    },
    iconContainer: {
        width: 30,
        height: 30,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    label: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        paddingRight: '10%',
        // padding: 15
    },
    text: {
        fontFamily: 'IRANSansMobile(FaNum)',
        position: 'absolute',
        bottom: 20,
        color: 'black',
        fontSize: 14
    },
    settingContainer: {
       flexDirection: 'row'
    },
    settingText: {
        color: 'rgb(22, 89, 149)',
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingTop: '6%',
        paddingRight: 10
    }
});

