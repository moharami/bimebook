
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput, ImageBackground,  Dimensions} from 'react-native';
import styles from './styles'
import tag from '../../assets/tag.png'
import {Actions} from 'react-native-router-flux'
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bani.azarinpro.info/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import PageHeader from '../../components/pageHeader'
import FooterMenu from '../../components/footerMenu'
import ReminderItem from '../../components/reminderItem'
import ReminderInput from '../../components/reminderInput'
import setting from '../../assets/setting.png';
import SettingModal from './settingModal'

class ReminderDetail extends Component {

    constructor(props){
        super(props);
        this.state = {
            loading: false,
            active: 1,
            modalVisible: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    setModalVisible() {
        this.setState({modalVisible: !this.state.modalVisible});
    }
    closeModal() {
        this.setState({modalVisible: false});

    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        } else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.topHeader}>
                            <PageHeader carInfo={false} title="یادآور بیمه" />
                        </View>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            <ReminderItem/>
                            <Text style={styles.label}>یادآوری از طریق پیامک</Text>
                            <ReminderInput />
                            <Text style={styles.label}>یادآوری از طریق ایمیل</Text>
                            <ReminderInput />
                            <Text style={styles.label}>یادآوری از طریق تماس تلفنی</Text>
                            <ReminderInput />
                            <TouchableOpacity onPress={()=> this.setState({modalVisible: !this.state.modalVisible})} style={styles.settingContainer}>
                                <Text style={styles.settingText}>تنظیمات یادآوری</Text>
                                <Image source={setting} style={{tintColor: 'rgb(22, 89, 149)', width: 20, resizeMode: 'contain'}} />
                            </TouchableOpacity>
                            <SettingModal
                                closeModal={() => this.closeModal()}
                                modalVisible={this.state.modalVisible}
                                onChange={(visible) => this.setModalVisible(visible)}
                            />
                        </View>
                    </ScrollView>
                    <FooterMenu active="home" />
                </View>
            );
    }
}
export default ReminderDetail;

