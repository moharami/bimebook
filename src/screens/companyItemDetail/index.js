
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput, ImageBackground,  Dimensions} from 'react-native';
import styles from './styles'
import headerBg from '../../assets/headerBg.png'
import {Actions} from 'react-native-router-flux'
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bani.azarinpro.info/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import FooterMenu from '../../components/footerMenu'
import CommentItem from '../../components/commentItem'
import asia from '../../assets/asia.png'
import bimeh_motor from '../../assets/insurance/bimeh-motor.png'
import bimeh_badaneh from '../../assets/insurance/bimeh-badaneh.png'
import bimeh_sales from '../../assets/insurance/bimeh-sales.png'
import CommentModal from './commentModal'

class CompanyItemDetail extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            nationalCard: '',
            third: '',
            newDoc: '',
            src1: null,
            src2: null,
            src3: null,
            commentModal: false,
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

    }
    closeModal() {
        this.setState({commentModal: false});
    }

    setModalVisible() {
        this.setState({commentModal: !this.state.commentModal});
    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        } else
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.topHeader}>
                        <View style={styles.headerTitleContainer}>
                            <TouchableOpacity onPress={() => this.onBackPress()}>
                                <FIcon name="chevron-left" size={30} color="white" />
                            </TouchableOpacity>
                            <Text style={styles.profileTitle}>بیمه آسیا</Text>
                        </View>
                    </View>
                    <Image source={headerBg} style={styles.headerImage} />
                </View>
                <View style={styles.imageContainer}>
                    <Image source={asia} style={{width: 75, height: 75, resizeMode: 'contain'}} />
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <View style={styles.body}>
                            <View style={styles.titleContainer}>
                                <View style={styles.branchContainer}>
                                    <Text style={styles.branchTxt}>مشاهده شعب</Text>
                                </View>
                                <Text style={styles.institle}>بیمه آسیا</Text>
                            </View>
                            <View style={styles.info}>
                                <View style={styles.branch}>
                                    <Text style={styles.value}>128</Text>
                                    <Text style={styles.label}>تعداد شعب</Text>
                                </View>
                                <View style={styles.branch}>
                                    <Text style={styles.value}>2</Text>
                                    <Text style={styles.label}>توانگری مالی</Text>
                                </View>
                                <View style={styles.branch}>
                                    <Text style={styles.value}>5 از 5</Text>
                                    <Text style={styles.label}>پاسخگویی</Text>
                                </View>
                                <View style={styles.branch}>
                                    <Text style={styles.value}>3.2 از 5</Text>
                                    <Text style={styles.label}>رضایت مشتریان</Text>
                                </View>
                            </View>
                            <Text style={styles.title}>درباره شرکت بیمه</Text>
                            <Text style={styles.content}>
                                بيـمه آسـيا به عنـوان يكي از معتبرترين شركت هاي بيمه با انجام عمليات بيـمه اي در زمينه هاي زندگي ، غير زندگي و اتكایي همواره بر آن است تا با بكارگيري روش هاي نوين ارائه خدمات و با اتكا بر سرمايه انساني متخصص در جهت تامين رضايت كليه ذينفعان تلاش كرده و با بهبود مستمر تركيب سبد خدمات حضوري موثر در بازارهاي داخلي و خارجي داشته باشد .
                            </Text>
                            <Text style={styles.title}>خدمات بیمه آسیا</Text>
                            <View style={styles.row}>
                                <View style={styles.item1}>
                                    <View  style={styles.imageContainer1}>
                                        <Image source={bimeh_motor} style={styles.bodyImage} />
                                        <Text style={styles.label1}>موتور سیکلت</Text>
                                    </View>
                                </View>
                                <View style={styles.item1}>
                                    <View style={styles.imageContainer1}>
                                        <Image source={bimeh_badaneh} style={styles.bodyImage} />
                                        <Text style={styles.label1}>بدنه</Text>
                                    </View>
                                </View>
                                <View style={styles.item1}>
                                    <View  style={styles.imageContainer1}>
                                        <Image source={bimeh_sales} style={styles.bodyImage} />
                                        <Text style={styles.label1}>شخص ثالث</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.commentContainer}>
                                <Text style={styles.title}>نظرات کاربران</Text>
                                <CommentItem />
                                <CommentItem />
                                <CommentItem />
                                <TouchableOpacity onPress={() => this.setState({commentModal: true})} style={styles.advertise}>
                                    <Text style={styles.buttonTitle}>ثبت نظر جدید</Text>
                                </TouchableOpacity>

                            </View>
                        </View>
                        <CommentModal
                            closeModal={() => this.closeModal()}
                            modalVisible={this.state.commentModal}
                            onChange={(visible) => this.setModalVisible(visible)}
                        />
                    </View>
                </ScrollView>
                <FooterMenu active="home" />
            </View>
        );
    }
}
export default CompanyItemDetail;

