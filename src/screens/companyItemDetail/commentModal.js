
import React, { Component } from 'react';
import {
    Modal,
    Text,
    View,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    AsyncStorage,
    Image,
    Alert
}
    from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome'

import Axios from 'axios'
// Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
// export const url = 'http://fitclub.ws';
// Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import asia from '../../assets/asia.png'

class CommentModal extends Component {
    state = {
      text: ''
    };
    onDateChange(date) {
        setTimeout(() => {this.setState({showPicker: false})}, 200)
        this.setState({ selectedStartDate: date });
    }
    render() {
        if(this.state.loading)
            return (<Loader txtColor="red" color='red' />);
        else return (
            <View style = {styles.container}>
                <Modal animationType = {"fade"} transparent = {true}
                       visible = {this.props.modalVisible}
                       onRequestClose = {() => this.props.onChange(false)}
                       onBackdropPress= {() => this.props.onChange(!this.state.modalVisible)}
                >
                    <View style = {styles.modal}>
                        <View style={styles.box}>
                            <View style={styles.headerContainer}>
                                <TouchableOpacity onPress={() => this.props.closeModal()}>
                                    <Icon name="close" size={20} color={ "black"} />
                                </TouchableOpacity>
                                <Text style={styles.headerText}>نظر و امتیاز</Text>
                            </View>
                            <Text style={styles.title}>امتیاز و نظر شما درباره بیمه آسیا</Text>
                            <View style={styles.info}>
                                <View style={styles.rowsContainer}>
                                    <View style={styles.row}>
                                        <View style={{flexDirection: 'row'}}>
                                            <Icon name="star" size={14} color={ "rgb(253, 185, 11)"} />
                                            <Icon name="star" size={14} color={ "rgb(253, 185, 11)"} />
                                            <Icon name="star" size={14} color={ "rgb(253, 185, 11)"} />
                                            <Icon name="star" size={14} color={ "rgb(253, 185, 11)"} />
                                        </View>
                                        <Text style={styles.title}>قیمت</Text>
                                    </View>
                                    <View style={styles.row}>
                                        <View style={{flexDirection: 'row'}}>
                                            <Icon name="star" size={14} color={ "rgb(253, 185, 11)"} />
                                        </View>
                                        <Text style={styles.title}>رضایت از پرداخت</Text>
                                    </View>
                                    <View style={styles.row}>
                                        <View style={{flexDirection: 'row'}}>
                                            <Icon name="star" size={14} color={ "rgb(253, 185, 11)"} />
                                            <Icon name="star" size={14} color={ "rgb(253, 185, 11)"} />
                                        </View>
                                        <Text style={styles.title}>پاسخگویی</Text>
                                    </View>
                                </View>
                                <View style={styles.imageContainer}>
                                    <Image source={asia} style={{width: 50, height: 50, resizeMode: 'contain'}} />
                                </View>
                            </View>
                            <TextInput
                                multiline = {true}
                                numberOfLines = {4}
                                value={this.state.text}
                                onChangeText={(text) => this.setState({text: text})}
                                placeholderTextColor={'rgb(142, 142, 142)'}
                                underlineColorAndroid='transparent'
                                placeholder="نظر شما..."
                                style={{
                                    // height: 45,
                                    paddingRight: 15,
                                    width: '90%',
                                    fontSize: 14,
                                    color: 'rgb(50, 50, 50)',
                                    textAlign:'right',
                                    fontFamily: 'IRANSansMobile(FaNum)',
                                    borderWidth: 1,
                                    borderColor: 'rgb(150, 150, 150)',
                                    borderRadius: 10,
                                    marginTop: 20,
                                    marginBottom: 20
                                }}
                            />
                            <TouchableOpacity onPress={() => null} style={styles.advertise}>
                                <Text style={styles.buttonTitle}>تایید</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </Modal>
            </View>
        )
    }
}
export default CommentModal

const styles = StyleSheet.create ({
    container: {
        alignItems: 'center',
        // backgroundColor: 'white',
        padding: 10,
        position: 'absolute',
        bottom: -200
    },
    modal: {
        position: 'relative',
        zIndex: 0,
        flexGrow: 1,
        justifyContent: 'center',
        // flex: 1,
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,.6)',
        paddingRight: 20,
        paddingLeft: 20
    },
    text: {
        color: '#3f2949',
        marginTop: 10
    },
    box: {
        width: '100%',
        backgroundColor: 'rgb(218, 218, 218)',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        borderRadius: 15,
        paddingBottom: 20
    },
    input: {
        width: '100%',
        fontSize: 16,
        paddingTop: 0,
        textAlign: 'right',
    },
    searchButton: {
        width: '90%',
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgb(20, 122, 170)'
    },
    searchText:{
        color: 'white'
    },
    picker: {
        height:50,
        width: "100%",
        alignSelf: 'flex-end'
    },
    label: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'rgb(60, 60, 60)'
    },
    headerContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 8,
        borderBottomWidth: 1,
        borderBottomColor: 'rgb(237, 237, 237)',
        marginBottom: 20
    },
    headerText: {
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    advertise: {
        width: '35%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#8dc63f',
        padding: 5,
        // marginTop: 0
    },
    buttonTitle: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    inputLabel: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        paddingRight: '10%',
        paddingBottom: 5
    },
    insContainer: {
        width: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        // alignItems: 'center',
        // justifyContent: 'space-between',
        padding: 8
    },
    info: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingTop: 20,
        paddingBottom: 20,
        borderBottomColor: 'white',
        borderBottomWidth: 1
    },
    rowsContainer: {
        width: '70%'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    itemContainer: {
        width: '48%'
    },
    title: {
        color: 'rgb(60, 60, 60)',
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        paddingRight: 10
    },
    imageContainer: {
        borderRadius: 15,
        padding: 7,
        backgroundColor: 'white',
        width: '20%',
        marginRight: 10,
        marginLeft: 10
    },
})