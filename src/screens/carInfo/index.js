
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput, ImageBackground,  Dimensions} from 'react-native';
import styles from './styles'
import tag from '../../assets/tag.png'
import {Actions} from 'react-native-router-flux'
import FIcon from 'react-native-vector-icons/dist/Feather';
import OIcon from 'react-native-vector-icons/dist/Octicons';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bani.azarinpro.info/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import PageHeader from '../../components/pageHeader'
import FooterMenu from '../../components/footerMenu'
import InsuranceInfo from '../../components/insuranceInfo'
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import Slider from "react-native-slider";

class CarInfo extends Component {

    constructor(props){
        super(props);
        this.state = {
            defaultItem: 2,
            area_price: 1,
            sliderChange: false,
            text1: '',
            text2: '',
            text3: '',
            loading: false,
            input1: '',
            shasiNumber: '',
            bodyNumber: '',
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

    }
    render() {
        const prices = ['7,700,000', '10,000,000', '15,000,000', '20,000,000', '25,000,000','30,000,000', '35,000,000'];
        const alphabet = ['الف', 'ب', 'پ', 'ت', 'ث', 'ج', 'چ', 'ح', 'خ', 'د', 'ذ', 'ر', 'ز', 'ژ', 'س', 'ش', 'ص', 'ض', 'ط', 'ظ', 'ع', 'غ', 'ف', 'ق', 'ک', 'ل', 'م', 'ن', 'و', 'ه', 'ی'];
        // const {posts, categories, user} = this.props;
        if(this.state.loading){
            return (<Loader />)
        } else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.topHeader}>
                            <PageHeader carInfo={false} title="مشخصات خودرو" />
                        </View>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            <View style={styles.tagImageContainer}>
                                <Image source={tag} style={styles.tagImage} />
                                <Text style={styles.tagTxt1}>161</Text>
                                <Text style={styles.tagTxt2}>ب</Text>
                                <Text style={styles.tagTxt3}>34</Text>
                                <Text style={styles.tagTxt4}>33</Text>
                            </View>
                            <Text style={styles.label}>شماره پلاک خود را به شکل صحیح وارد نمایید:</Text>
                            <View style={{
                                display: 'flex',
                                width: '100%',
                                flexDirection: 'row',
                                justifyContent: 'space-around',
                                alignContent: 'space-between',
                                padding: 30
                            }}>
                                <TextInput
                                    keyboardType='numeric'
                                    placeholderTextColor={'#C8C8C8'}
                                    underlineColorAndroid='transparent'
                                    style={{
                                        textAlign: 'center',
                                        borderWidth: 1,
                                        borderColor: 'gray',
                                        height: 45,
                                        backgroundColor: 'white',
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        flex: .15,
                                        borderRadius: 21,
                                        fontSize: 18,
                                        color: '#7A8299'
                                    }}
                                    value={this.state.text1}
                                    onChangeText={(text) => {
                                        this.setState({text1: text})
                                        // this.thirdTextInput.focus()
                                    }}/>
                                <View style={{position: 'relative', zIndex: 1, width: '20%',   borderWidth: 1,
                                    borderColor: 'gray',
                                    // height: 45,
                                    backgroundColor: 'white',
                                    borderRadius: 21,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}>
                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '25%', right: '8%'}}/>
                                    <Picker
                                        mode="dropdown"
                                        style={[styles.picker, {backgroundColor: 'white', width: '90%'}]}
                                        selectedValue={this.state.input1}
                                        // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                        onValueChange={(itemValue) =>{
                                            this.setState({
                                                input1: itemValue
                                            })}}>
                                        {
                                            alphabet.map((item, index) => <Picker.Item key={index} label={item} value={index} />
                                            )
                                        }
                                    </Picker>
                                </View>
                                <TextInput
                                    keyboardType='numeric'
                                    placeholderTextColor={'gray'}
                                    underlineColorAndroid='transparent'
                                    value={this.state.text2}
                                    ref={(input) => {
                                        this.thirdTextInput = input
                                    }}
                                    style={{
                                        textAlign: 'center',
                                        borderWidth: 1,
                                        borderColor: 'gray',
                                        height: 45,
                                        backgroundColor: 'white',
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        flex: .15,
                                        borderRadius: 21,
                                        fontSize: 18,
                                        color: 'gray'
                                    }}
                                    onChangeText={(text) => {
                                        this.setState({text2: text})
                                    }}/>
                                <OIcon name="dash" size={20} color="lightgray" style={{paddingRight: 3, borderRadius: 10, paddingTop: 10}} />
                                <TextInput
                                    keyboardType='numeric'
                                    placeholderTextColor={'gray'}
                                    underlineColorAndroid='transparent'
                                    value={this.state.text3}
                                    ref={(input) => {
                                        this.fourthTextInput = input
                                    }}
                                    style={{
                                        textAlign: 'center',
                                        borderWidth: 1,
                                        borderColor: 'gray',
                                        height: 45,
                                        backgroundColor: 'white',
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        flex: .15,
                                        borderRadius: 21,
                                        fontSize: 18,
                                        color: '#7A8299'
                                    }}
                                    onChangeText={(text) => {
                                        this.setState({text3: text})

                                    }}/>
                            </View>
                            {
                                this.state.shasiNumber.length !== 0 ?  <Text style={styles.inputLabel}>شماره شاسی</Text> : null

                            }
                            <TextInput
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                placeholder='شماره شاسی'
                                value={this.state.shasiNumber}
                                style={{
                                    textAlign: 'right',
                                    borderWidth: 1,
                                    borderColor: 'gray',
                                    height: 50,
                                    backgroundColor: 'white',
                                    paddingRight: 10,
                                    paddingLeft: 10,
                                    width: '80%',
                                    borderRadius: 25,
                                    fontSize: 14,
                                    color: '#7A8299',
                                    marginBottom: 20
                                }}
                                onChangeText={(text) => {
                                    this.setState({shasiNumber: text})

                                }}/>
                            {
                                this.state.bodyNumber.length !== 0 ?  <Text style={styles.inputLabel}>شماره بدنه</Text> : null

                            }
                            <TextInput
                                placeholderTextColor={'gray'}
                                placeholder={'شماره بدنه'}
                                underlineColorAndroid='transparent'
                                value={this.state.bodyNumber}
                                style={{
                                    textAlign: 'right',
                                    borderWidth: 1,
                                    borderColor: 'gray',
                                    height: 50,
                                    backgroundColor: 'white',
                                    paddingRight: 10,
                                    paddingLeft: 10,
                                    width: '80%',
                                    borderRadius: 25,
                                    fontSize: 14,
                                    color: '#7A8299',
                                }}
                                onChangeText={(text) => {
                                    this.setState({bodyNumber: text})

                                }}/>
                            <TouchableOpacity onPress={() => Actions.home()} style={styles.advertise}>
                                <Text style={styles.buttonTitle}>ثبت اطلاعات خودرو</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                    <View style={styles.reminder}>
                        <Icon name="bell-o" size={25} color="white" />
                        <Text style={styles.reminderText}>یادآور بیمه</Text>
                    </View>
                    <FooterMenu active="home" />
                </View>
            );
    }
}
export default CarInfo;

