
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput, ImageBackground,  Dimensions} from 'react-native';
import styles from './styles'
import headerBg from '../../assets/headerBg.png'
import {Actions} from 'react-native-router-flux'
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bani.azarinpro.info/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import PageHeader from '../../components/pageHeader'
import FooterMenu from '../../components/footerMenu'
import InsuranceInfo from '../../components/insuranceInfo'
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import Slider from "react-native-slider";

class Results extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            defaultItem: 2,
            area_price: 1,
            sliderChange: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

    }
    onDateChange(date) {
        setTimeout(() => {this.setState({showPicker: false})}, 200)
        this.setState({ selectedStartDate: date });
    }
    slidingComplete(itemSelected) {
        console.log('item', itemSelected)
      this.setState({defaultItem: itemSelected})
    }
    render() {
        const sliderOptions = [
            {value: 0, label: 'item A'},
            {value: 1, label: 'item B'},
            {value: 2, label: 'item C'},
            {value: 3, label: 'item D'},
        ];
        const prices = ['7,700,000', '10,000,000', '15,000,000', '20,000,000', '25,000,000','30,000,000', '35,000,000'];
        // const {posts, categories, user} = this.props;
        if(this.state.loading){
            return (<Loader />)
        } else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.topHeader}>
                            <PageHeader  title="نتایج" />
                        </View>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            <View style={styles.headerContainer}>
                                <Text style={styles.headerText}>سطح تعهدات مالی(تومان)</Text>
                                <Slider
                                    style={{
                                        flex: 1,
                                        width: '90%',
                                        marginLeft: 10,
                                        marginTop: 20,
                                        marginRight: 10,
                                        alignItems: "stretch",
                                        justifyContent: "center"
                                    }}
                                    minimumValue={1}
                                    maximumValue={7}
                                    maximumTrackTintColor="#00b3bc"
                                    // minimumTrackTintColor="red"
                                    step={1}
                                    value={this.state.area_price}
                                    onValueChange={value => this.setState({area_price: value, sliderChange: true})}/>
                                <Text style={[styles.headerText, {alignSelf: 'center'}]}>{prices[this.state.area_price-1]}</Text>
                            </View>
                            <View style={styles.insuranceContainer}>
                                <InsuranceInfo />
                                <InsuranceInfo />
                                <InsuranceInfo />
                            </View>
                        </View>
                    </ScrollView>
                    <View style={styles.reminder}>
                        <Icon name="bell-o" size={25} color="white" />
                        <Text style={styles.reminderText}>یادآور بیمه</Text>
                    </View>
                    <FooterMenu active="home" />
                </View>
            );
    }
}
export default Results;

