
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput, ImageBackground,  Dimensions} from 'react-native';
import styles from './styles'
import headerBg from '../../assets/headerBg.png'
import {Actions} from 'react-native-router-flux'
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bani.azarinpro.info/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import PageHeader from '../../components/pageHeader'
import FooterMenu from '../../components/footerMenu'
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import AppSearchBar from '../../components/searchBar'
import CompanyItem from "../../components/companyItem/index";
import CourierItem from "../../components/courierItem";

class Courier extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            defaultItem: 2,
            area_price: 1,
            sliderChange: false,
            active: 1
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        } else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.topHeader}>
                            <PageHeader title="پیک" />
                        </View>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            {/*<AppSearchBar />*/}
                            <View style={styles.searchContainer}>
                                <TextInput
                                    maxLength={15}
                                    placeholder={'جستجوی سفارشات...'}
                                    placeholderTextColor={'gray'}
                                    underlineColorAndroid='transparent'
                                    value={this.state.text}
                                    style={{
                                        height: 35,
                                        width: '90%',
                                        color: 'gray',
                                        fontSize: 14,
                                        textAlign: 'right',
                                        // elevation: 4,
                                        shadowColor: 'lightgray',
                                        borderRadius: 20,
                                        backgroundColor: 'white',
                                        // elevation: 5,
                                        borderWidth:1,
                                        borderColor: 'black',
                                        paddingRight: 10
                                    }}
                                    onChangeText={(text) => this.setState({text: text})}
                                />
                                <TouchableOpacity onPress={() => this.startSearch(this.state.text)} style={{position: 'absolute', zIndex: 9992, top: 17, left: 30}}>
                                    <FIcon name="search" size={20} color="gray"  />
                                </TouchableOpacity>
                            </View>

                            <View style={styles.bordered}>
                                <ScrollView style={{ transform: [
                                    { scaleX: -1}
                                ], width: '100%'}}
                                            horizontal={true} showsHorizontalScrollIndicator={false}
                                >
                                    <View style={styles.catContainer}>
                                        <TouchableOpacity onPress={() => this.setState({active: 3})}>
                                            <View style={[styles.navContainer, {borderBottomWidth: this.state.active === 3 ? 4 : 0, borderBottomColor: '#8dc63f'}]}>
                                                <Text style={styles.text}>بدنه</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.setState({active: 2})}>
                                            <View style={[styles.navContainer, {borderBottomWidth: this.state.active === 2 ? 4 : 0, borderBottomColor: '#8dc63f'}]}>
                                                <Text style={styles.text}>شخص ثالث</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.setState({active: 1})}>
                                            <View style={[styles.navContainer, {borderBottomWidth: this.state.active === 1 ? 4 : 0, borderBottomColor: '#8dc63f'}]}>
                                                <Text style={styles.text}>همه موارد</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </ScrollView>
                            </View>
                            <View style={styles.body}>
                                <CourierItem />
                                <CourierItem />
                            </View>
                        </View>
                    </ScrollView>
                    <FooterMenu active="company" />
                </View>
            );
    }
}
export default Courier;

