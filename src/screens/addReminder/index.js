
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bani.azarinpro.info/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import PageHeader from '../../components/pageHeader'
import FooterMenu from '../../components/footerMenu'
import setting from '../../assets/setting.png';
import InsuranceModal from './insuranceModal'
import SettingModal from './settingModal'
import bimeh_tamin from '../../assets/insurance/bimeh-tamin.png'
import bimeh_darman from '../../assets/insurance/bimeh-darman.png'
import bimeh_mosaferat from '../../assets/insurance/bimeh-mosaferat.png'
import bimeh_masooliat from '../../assets/insurance/bimeh-masooliat.png'
import bimeh_zelzele from '../../assets/insurance/bimeh-zelzele.png'
import bimeh_atashsoozi from '../../assets/insurance/bimeh-atashsoozi.png'
import bimeh_omr from '../../assets/insurance/bimeh-omr.png'
import bimeh_motor from '../../assets/insurance/bimeh-motor.png'
import bimeh_badaneh from '../../assets/insurance/bimeh-badaneh.png'
import bimeh_sales from '../../assets/insurance/bimeh-sales.png'
import FIcon from 'react-native-vector-icons/dist/Feather';
import PersianCalendarPicker from 'react-native-persian-calendar-picker';
import moment_jalaali from 'moment-jalaali'

class AddReminder extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            modalVisible: false,
            insuranceModal: false,
            selectedStartDate: null,
            active: 0,
            showPicker: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    setModalVisible() {
        this.setState({modalVisible: !this.state.modalVisible});
    }
    setModalVisibleInsurance() {
        this.setState({insuranceModal: !this.state.insuranceModal});
    }
    closeModal() {
        this.setState({modalVisible: false});

    }
    closeModalInsurance() {
        this.setState({insuranceModal: false});
    }
    onDateChange(date) {
        setTimeout(() => {this.setState({showPicker: false})}, 100);
        this.setState({ selectedStartDate: date });
    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        } else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.topHeader}>
                            <PageHeader carInfo={false} title="اضافه کردن یادآوری" />
                        </View>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            <Text style={styles.typeLabel}>نوع بیمه</Text>
                            <View  style={styles.row}>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={() => this.setState({active: 1})} style={[styles.imageContainer, {backgroundColor: this.state.active === 1 ? 'rgb(11, 81, 144)' : 'white'}]}>
                                        <Image source={bimeh_motor} style={[styles.bodyImage, {tintColor: this.state.active === 1 ? 'white' : 'rgb(11, 81, 144)'}]} />
                                        <Text style={[styles.label, {color: this.state.active ===1 ? 'white' : undefined}]}>موتور سیکلت</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={() => this.setState({active: 2})} style={[styles.imageContainer, {backgroundColor: this.state.active === 2 ? 'rgb(11, 81, 144)' : 'white'}]}>
                                        <Image source={bimeh_badaneh} style={[styles.bodyImage, {tintColor: this.state.active === 2 ? 'white' : 'rgb(11, 81, 144)'}]} />
                                        <Text style={[styles.label, {color: this.state.active ===2 ? 'white' : undefined}]}>بدنه</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={() => this.setState({active: 3})} style={[styles.imageContainer, {backgroundColor: this.state.active === 3 ? 'rgb(11, 81, 144)' : 'white'}]}>
                                        <Image source={bimeh_sales} style={[styles.bodyImage, {tintColor: this.state.active === 3 ? 'white' : 'rgb(11, 81, 144)'}]} />
                                        <Text style={[styles.label, {color: this.state.active ===3 ? 'white' : undefined}]}>شخص ثالث</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={() => this.setState({active: 4})} style={[styles.imageContainer, {backgroundColor: this.state.active === 4 ? 'rgb(11, 81, 144)' : 'white'}]}>
                                        <Image source={bimeh_zelzele} style={[styles.bodyyImage, {tintColor: this.state.active === 4 ? 'white' : 'rgb(11, 81, 144)'}]} />
                                        <Text style={[styles.label, {color: this.state.active ===4 ? 'white' : undefined}]}>زلزله</Text>

                                    </TouchableOpacity>
                                </View>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={() => this.setState({active: 5})} style={[styles.imageContainer, {backgroundColor: this.state.active === 5 ? 'rgb(11, 81, 144)' : 'white'}]}>
                                        <Image source={bimeh_atashsoozi} style={[styles.bodyyImage, {tintColor: this.state.active === 5 ? 'white' : 'rgb(11, 81, 144)'}]} />
                                        <Text style={[styles.label, {color: this.state.active ===5 ? 'white' : undefined}]}>آتش سوزی</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View  style={styles.row}>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={() => this.setState({active: 6})} style={[styles.imageContainer, {backgroundColor: this.state.active === 6 ? 'rgb(11, 81, 144)' : 'white'}]}>
                                        <Image source={bimeh_omr} style={[styles.bodyImage, {tintColor: this.state.active === 6 ? 'white' : 'rgb(11, 81, 144)'}]} />
                                        <Text style={[styles.label, {color: this.state.active ===6 ? 'white' : undefined}]}>عمر</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={() => this.setState({active: 7})} style={[styles.imageContainer, {backgroundColor: this.state.active === 7 ? 'rgb(11, 81, 144)' : 'white'}]}>
                                        <Image source={bimeh_darman} style={[styles.bodyyImage, {tintColor: this.state.active === 7 ? 'white' : 'rgb(11, 81, 144)'}]} />
                                        <Text style={[styles.label, {color: this.state.active ===7 ? 'white' : undefined}]}>درمانی(تکمیلی)</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={() => this.setState({active: 8})} style={[styles.imageContainer, {backgroundColor: this.state.active === 8 ? 'rgb(11, 81, 144)' : 'white'}]}>
                                        <Image source={bimeh_masooliat} style={[styles.bodyyImage, {tintColor: this.state.active === 8 ? 'white' : 'rgb(11, 81, 144)'}]} />
                                        <Text style={[styles.label, {color: this.state.active === 8 ? 'white' : undefined}]}>مسئولیت</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={() => this.setState({active: 9})} style={[styles.imageContainer, {backgroundColor: this.state.active === 9 ? 'rgb(11, 81, 144)' : 'white'}]}>
                                        <Image source={bimeh_mosaferat} style={[styles.bodyyImage, {tintColor: this.state.active === 9 ? 'white' : 'rgb(11, 81, 144)'}]} />
                                        <Text style={[styles.label, {color: this.state.active === 9 ? 'white' : undefined}]}>مسافرت</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={() => this.setState({active: 10})} style={[styles.imageContainer, {backgroundColor: this.state.active === 10 ? 'rgb(11, 81, 144)' : 'white'}]}>
                                        <Image source={bimeh_tamin} style={[styles.bodyyImage, {tintColor: this.state.active === 10 ? 'white' : 'rgb(11, 81, 144)'}]} />
                                        <Text style={[styles.label, {color: this.state.active === 10 ? 'white' : undefined}]}>تامین اجتماعی</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={styles.innerPickerContainer}>
                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                    <TouchableOpacity onPress={() => this.setState({insuranceModal: true})} style={{position: 'absolute', zIndex: 90, top: 6, left: 7}}>
                                        <FIcon name="chevron-down" size={20} color="gray" />
                                    </TouchableOpacity>
                                    <TextInput
                                        // onFocus={() =>{
                                        //     this.setState({
                                        //         insuranceModal: true
                                        //     })}}
                                        placeholder='شرکت بیمه'
                                        placeholderTextColor={'gray'}
                                        underlineColorAndroid='transparent'
                                        value={this.state.selectedStartDate !==null ? moment_jalaali(this.state.selectedStartDate).format('jYYYY/jM/jD') :null}
                                        style={{
                                            height: 36,
                                            marginBottom: 10,
                                            // paddingRight: 25,
                                            width: '100%',
                                            color: 'rgb(50, 50, 50)',
                                            fontSize: 14,
                                            textAlign: 'right'
                                        }}
                                    />
                                </View>
                            </View>
                            <View style={styles.innerPickerContainer}>
                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                    <TouchableOpacity onPress={() => this.setState({  showPicker: true})} style={{position: 'absolute', zIndex: 90, top: 6, left: 7}}>
                                        <FIcon name="calendar" size={20} color="gray" />
                                    </TouchableOpacity>
                                    <TextInput
                                        // onFocus={() =>{
                                        //     this.setState({
                                        //         showPicker: true
                                        //     })}}
                                        placeholder='تاریخ سررسید'
                                        placeholderTextColor={'gray'}
                                        underlineColorAndroid='transparent'
                                        value={this.state.selectedStartDate !==null ? moment_jalaali(this.state.selectedStartDate).format('jYYYY/jM/jD') :null}
                                        style={{
                                            height: 36,
                                            marginBottom: 10,
                                            // paddingRight: 25,
                                            width: '100%',
                                            color: 'rgb(50, 50, 50)',
                                            fontSize: 14,
                                            textAlign: 'right'
                                        }}
                                    />
                                </View>
                            </View>
                            <TouchableOpacity onPress={()=> this.setState({modalVisible: !this.state.modalVisible})} style={styles.settingContainer}>
                                <Text style={styles.settingText}>تنظیمات یادآوری</Text>
                                <Image source={setting} style={{tintColor: 'rgb(22, 89, 149)', width: 20, resizeMode: 'contain'}} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => Actions.home()} style={styles.advertise}>
                                <Text style={styles.buttonTitle}>اضافه کن! </Text>
                            </TouchableOpacity>
                            <SettingModal
                                closeModal={() => this.closeModal()}
                                modalVisible={this.state.modalVisible}
                                onChange={(visible) => this.setModalVisible(visible)}
                            />
                            <InsuranceModal
                                closeModal={() => this.closeModalInsurance()}
                                modalVisible={this.state.insuranceModal}
                                onChange={(visible) => this.setModalVisibleInsurance(visible)}
                            />

                            {
                                this.state.showPicker ?
                                    <View style={{
                                        position: 'absolute',
                                        bottom: '50%',
                                        zIndex: 9999,
                                        backgroundColor: 'white'
                                    }}>
                                        <PersianCalendarPicker
                                            onDateChange={(date) => this.onDateChange(date)}
                                        />
                                    </View>
                                    : null
                            }
                        </View>
                    </ScrollView>
                    <FooterMenu active="home" />
                </View>
            );
    }
}
export default AddReminder;

