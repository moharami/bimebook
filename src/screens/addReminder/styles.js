
import { StyleSheet, Dimensions} from 'react-native';
// import env from '../../colors/env';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(246, 246, 246)',
        // backgroundColor: 'red'
    },
    header: {
        flex: 1,
        height: '10%',
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9990,
        backgroundColor: 'rgb(247, 247, 247)'
    },
    topHeader: {
        backgroundColor: 'rgb(20, 85, 151)',
        width: '100%',
        height: '100%'
    },
    headerImage: {
        width: '100%'
    },
    headerTitle: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: '19%'
    },
    bodyContainer: {
        width: '100%',
        paddingBottom: 200,
        backgroundColor: 'rgb(248, 248, 248)',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 15
    },
    scroll: {
        // paddingTop: 10,
        backgroundColor: 'rgb(249, 249, 249)',
        paddingTop: '18%',

    },
    iconContainer: {
        width: 30,
        height: 30,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    typeLabel: {
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        // paddingRight: '10%',
    },
    label: {
        fontSize: 9,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingBottom: 6
    },
    text: {
        fontFamily: 'IRANSansMobile(FaNum)',
        position: 'absolute',
        bottom: 20,
        color: 'black',
        fontSize: 14
    },
    settingContainer: {
        flexDirection: 'row'
    },
    settingText: {
        color: 'rgb(22, 89, 149)',
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingTop: '6%',
        paddingRight: 10
    },
    row: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingBottom: 10
    },
    lastrow: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 10,
        // paddingRight: '10%',
        // paddingLeft: '10%',
    },
    item: {
        width: '18%',
        paddingTop: 13,
        paddingBottom: 13,
        borderRadius: 13
    },
    imageContainer: {
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        // padding: 20,
        borderRadius: 13,
        elevation: 8
    },
    bodyImage: {
        width: 35,
        height: 35,
        resizeMode: 'contain',
        tintColor: 'rgb(20, 85, 151)'
    },
    bodyyImage: {
        width: 24,
        height: 24,
        resizeMode: 'contain',
        tintColor: 'rgb(20, 85, 151)',
        marginTop: 13
        // marginTop: 15,
        // marginBottom: 15,
    },
    innerPickerContainer: {
        width: '90%',
        height: 40,
        borderRadius: 20,
        borderWidth: 1,
        borderColor: 'rgb(200, 200, 200)',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        // backgroundColor: 'white'
    },
    picker: {
        height: 30,
        backgroundColor: 'rgb(248, 248, 248)',
        width: '108%',
        color: 'gray',
        borderColor: 'lightgray',
        borderWidth: 1,
        // elevation: 4,
        borderRadius: 15,
        marginBottom: 20,
        // overflow: 'hidden'
    },
    advertise: {
        width: '40%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#8dc63f',
        padding: 8,
        marginTop: 20
    },
    buttonTitle: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
});

