
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput, ImageBackground,  Dimensions} from 'react-native';
import styles from './styles'
import headerBg from '../../assets/headerBg.png'
import {Actions} from 'react-native-router-flux'
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bani.azarinpro.info/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import HomeHeader from '../../components/homeHeader'
import FooterMenu from '../../components/footerMenu'
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
import vacle1 from "../../assets/vacle/vacle1.png";
import vacle2 from "../../assets/vacle/vacle2.png";
import vacle3 from "../../assets/vacle/vacle3.png";
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import moment_jalaali from 'moment-jalaali'
import PersianCalendarPicker from 'react-native-persian-calendar-picker';
import SwitchRow from '../../components/switchRow'

class VacleInfo extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            activeVacle: 0,
            vacleType:0,
            name:0,
            nameTitle:null,
            year:0,
            yearTitle:null,
            damageNumber:0,
            damageNumberTitle:null,
            damageYear:0,
            damageYearTitle:0,
            usage:0,
            usageTitle:null,
            activeButton:0,
            selectedStartDate:null,
            vaclePrice:null,
            bodyThirdBime:0,
            bodyBime:0,
            Chemical:false,
            break_glass:false,
            natural_disaster:false,
            Steal:false,
            Price:false,
            Transit:false,
            transfer:false,
            feature:[],
            vacleId: null
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

    }
    onDateChange(date) {
        setTimeout(() => {this.setState({showPicker: false})}, 200)
        this.setState({ selectedStartDate: date });

    }
    switchButtons(status, id, title) {
        console.log(status)
        if(id === 1) {
            this.setState({
                Chemical: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 2) {
            this.setState({
                break_glass: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 3) {
            this.setState({
                natural_disaster: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 4) {
            this.setState({
                Steal: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 5) {
            this.setState({
                Price: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 6) {
            this.setState({
                Transit: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 7) {
            this.setState({
                transfer: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
    }
    render() {
        const times = this.state.times;
        // const {posts, categories, user} = this.props;
        if(this.state.loading){
            return (<Loader />)
        } else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.topHeader}>
                            <HomeHeader />
                        </View>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            <View style={styles.headerContainer}>
                                <Text style={styles.headerText}>{this.props.pageTitle}</Text>
                            </View>
                            {
                                this.props.motor ? null :
                                    <View>
                                        <Text style={styles.txt}>نوع وسیله نقلیه</Text>
                                        <View style={styles.itemContainer}>
                                            <View style={styles.vacleContainer}>
                                                <TouchableOpacity style={[styles.vacle, {backgroundColor: this.state.activeVacle === 3 ? 'white': 'transparent', elevation: this.state.activeVacle === 3 ? 4 : 0}]} onPress={()=>{
                                                    this.setState({
                                                        activeVacle: 3
                                                    })}}>
                                                    <Text style={styles.vacleText}>موتور</Text>
                                                    <Image source={vacle3} style={[styles.Image, {tintColor: this.state.activeVacle === 3 ?  'rgb(15, 76, 183)' : undefined}]} />
                                                </TouchableOpacity>
                                                <TouchableOpacity style={[styles.vacle, {backgroundColor: this.state.activeVacle === 2 ? 'white': 'transparent', elevation: this.state.activeVacle === 2 ? 4 : 0}]} onPress={()=>{
                                                    this.setState({
                                                        activeVacle: 2
                                                    })}}>
                                                    <Text style={styles.vacleText}>کامیون</Text>
                                                    <Image source={vacle2} style={[styles.Image, {tintColor:  this.state.activeVacle === 2 ?  'rgb(15, 76, 183)' : undefined}]} />
                                                </TouchableOpacity>
                                                <TouchableOpacity style={[styles.vacle, {backgroundColor: this.state.activeVacle === 1 ? 'white': 'transparent', elevation: this.state.activeVacle === 1 ? 4 : 0}]} onPress={()=>{
                                                    this.setState({
                                                        activeVacle: 1
                                                    })}}>
                                                    <Text style={styles.vacleText}>سواری</Text>
                                                    <Image source={vacle1} style={[styles.Image, {tintColor: this.state.activeVacle === 1 ?  'rgb(15, 76, 183)' : "gray"}]} />
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                            }
                            <View style={[styles.itemContainer, {marginTop: this.props.motor ? 20 : 0 }]}>
                                <View style={styles.pickerContainer}>
                                    <View style={styles.innerPickerContainer}>
                                        <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, right: 10}}/>
                                            <Picker
                                                mode="dropdown"
                                                style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                selectedValue={this.state.name}
                                                // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                onValueChange={(itemValue) =>{
                                                    this.setState({
                                                        vacleType: itemValue
                                                    })}}>
                                                <Picker.Item label={this.state.loading ? "در حال بارگذاری..." : "مدل وسیله نقلیه"} value={0} />
                                                <Picker.Item  label='sdew' value={1} />
                                            </Picker>
                                        </View>
                                    </View>
                                    <View style={styles.innerPickerContainer}>
                                        <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                            <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, right: 10}}/>
                                            <Picker
                                                mode="dropdown"
                                                style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                selectedValue={this.state.name}
                                                // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                onValueChange={(itemValue) =>{
                                                    this.setState({
                                                        name: itemValue
                                                    })}}>
                                                <Picker.Item label={this.state.loading ? "در حال بارگذاری..." : "نام وسیله نقلیه"} value={0} />
                                                <Picker.Item  label='sdew' value={1} />
                                            </Picker>
                                        </View>
                                    </View>
                                </View>
                            </View>
                                <View style={styles.itemContainer}>
                                    <View style={styles.pickerContainer}>
                                        {
                                            this.props.thirdBime ?
                                                <View style={styles.innerPickerContainer}>
                                                    <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                        {/*<FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, right: 10}}/>*/}
                                                        <TextInput
                                                            onFocus={() =>{
                                                                this.setState({
                                                                    showPicker: true
                                                                })}}
                                                            placeholder='تاریخ انقضای بیمه نامه قبلی'
                                                            placeholderTextColor={'gray'}
                                                            underlineColorAndroid='transparent'
                                                            value={this.state.selectedStartDate !==null ? moment_jalaali(this.state.selectedStartDate).format('jYYYY/jM/jD') :null}
                                                            style={{
                                                                height: 36,
                                                                marginBottom: 10,

                                                                backgroundColor: 'white',
                                                                // paddingRight: 25,
                                                                width: '100%',
                                                                color: 'gray',
                                                                fontSize: 14,
                                                                textAlign: 'right'
                                                            }}
                                                        />
                                                    </View>
                                                </View> :
                                                <View style={styles.innerPickerContainer}>
                                                    <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                        <TextInput
                                                            placeholder='ارزش خودرو'
                                                            placeholderTextColor={'gray'}
                                                            underlineColorAndroid='transparent'
                                                            value={this.state.vaclePrice}
                                                            style={{
                                                                height: 36,
                                                                marginBottom: 10,
                                                                backgroundColor: 'white',
                                                                paddingRight: 25,
                                                                width: '100%',
                                                                color: 'gray',
                                                                fontSize: 14,
                                                                textAlign: 'right',
                                                                // elevation: 4,
                                                                shadowColor: 'lightgray',
                                                                shadowOffset: {width: 5,height: 5},
                                                                shadowOpacity: 1 ,
                                                            }}
                                                             onChangeText={(text) =>this.setState({vaclePrice: text})}
                                                        />
                                                    </View>
                                                </View>

                                        }
                                        <View style={styles.innerPickerContainer}>
                                            <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, left: 10}}/>
                                                <Picker
                                                    mode="dropdown"
                                                    style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                    selectedValue={this.state.name}
                                                    // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                    onValueChange={(itemValue) =>{
                                                        this.setState({
                                                            year: itemValue
                                                        })}}>
                                                    <Picker.Item label="سال ساخت" value={0} />
                                                    <Picker.Item  label='sdew' value={1} />
                                                </Picker>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            {
                                this.props.thirdBime ?
                                    <View style={styles.itemContainer}>
                                        <View style={styles.pickerContainer}>
                                            <View style={styles.innerPickerContainer}>
                                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, left: 10}}/>
                                                    <Picker
                                                        mode="dropdown"
                                                        style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                        selectedValue={this.state.name}
                                                        // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                        onValueChange={(itemValue) =>{
                                                            this.setState({
                                                                damageNumber: itemValue
                                                            })}}>
                                                        <Picker.Item label="تعداد خسارت" value={0} />
                                                        <Picker.Item  label='sdew' value={1} />
                                                    </Picker>
                                                </View>
                                            </View>
                                            <View style={styles.innerPickerContainer}>
                                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, right: 10}}/>
                                                    <Picker
                                                        mode="dropdown"
                                                        style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                        selectedValue={this.state.name}
                                                        // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                        onValueChange={(itemValue) =>{
                                                            this.setState({
                                                                damageYear: itemValue
                                                            })}}>
                                                        <Picker.Item label="سال های عدم خسارت در بیمه قبلی" value={0} />
                                                        <Picker.Item  label='sdew' value={1} />
                                                    </Picker>
                                                </View>
                                            </View>
                                        </View>
                                    </View> : null
                            }
                            {
                                this.props.thirdBime ?
                                    <View style={styles.itemContainer}>
                                        <View style={styles.pickerContainer}>
                                            <View style={styles.innerPickerContainer}>
                                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, right: 10}}/>
                                                    <Picker
                                                        mode="dropdown"
                                                        style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                        selectedValue={this.state.name}
                                                        // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                        onValueChange={(itemValue) =>{
                                                            this.setState({
                                                                activeButton: itemValue
                                                            })}}>
                                                        <Picker.Item label="حداکثر پوشش بیمه" value={0} />
                                                        <Picker.Item  label='sdew' value={1} />
                                                    </Picker>
                                                </View>
                                            </View>
                                            <View style={styles.innerPickerContainer}>
                                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, left: 10}}/>
                                                    <Picker
                                                        mode="dropdown"
                                                        style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                        selectedValue={this.state.name}
                                                        // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                        onValueChange={(itemValue) =>{
                                                            this.setState({
                                                                usage: itemValue
                                                            })}}>
                                                        <Picker.Item label="کاربری" value={0} />
                                                        <Picker.Item  label='sdew' value={1} />
                                                    </Picker>
                                                </View>
                                            </View>
                                        </View>
                                    </View> : null
                            }
                            {
                                this.props.bodyBime ?
                                    <View style={styles.itemContainer}>
                                        <View style={styles.pickerContainer}>
                                            <View style={styles.innerPickerContainer}>
                                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, right: 10}}/>
                                                    <Picker
                                                        mode="dropdown"
                                                        style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                        selectedValue={this.state.name}
                                                        // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                        onValueChange={(itemValue) =>{
                                                            this.setState({
                                                                bodyBime: itemValue
                                                            })}}>
                                                        <Picker.Item label="سال های عدم خسارت در بدنه" value={0} />
                                                        <Picker.Item  label='sdew' value={1} />
                                                    </Picker>
                                                </View>
                                            </View>
                                            <View style={styles.innerPickerContainer}>
                                                <View style={{position: 'relative', zIndex: 3, width: '90%', height: 30}}>
                                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 6, right: 10}}/>
                                                    <Picker
                                                        mode="dropdown"
                                                        style={[styles.picker, {position: 'absolute', zIndex: 50}]}
                                                        selectedValue={this.state.name}
                                                        // onValueChange={itemValue => {this.test(); this.setState({name: itemValue })}}>
                                                        onValueChange={(itemValue) =>{
                                                            this.setState({
                                                                bodyThirdBime: itemValue
                                                            })}}>
                                                        <Picker.Item label="سال های عدم خسارت در شخص ثالث" value={0} />
                                                        <Picker.Item  label='sdew' value={1} />
                                                    </Picker>
                                                </View>
                                            </View>
                                        </View>
                                    </View> : null
                            }
                            {
                                this.props.bodyBime ?
                                    <View style={{paddingRight: 25, paddingLeft: 25}}>
                                        <SwitchRow label="پاشیدن مواد شیمیایی و اسیدی" switchButtons={(status) => this.switchButtons(status, 1, 'chemical')} />
                                        <SwitchRow label="شکست شیشه" switchButtons={(status) => this.switchButtons(status, 2, 'break_glass')}/>
                                        <SwitchRow label="سیل، زلزله و بلایای طبیعی" switchButtons={(status) => this.switchButtons(status, 3, 'natural_disaster')} />
                                        <SwitchRow label="سرقت درجا" switchButtons={(status) => this.switchButtons(status, 4, 'steal')} />
                                        <SwitchRow label="نوسانات ارزش بازار" switchButtons={(status) => this.switchButtons(status, 5, 'price')} />
                                        <SwitchRow label="ایاب و ذهاب" switchButtons={(status) => this.switchButtons(status, 6, 'transit')} />
                                        <SwitchRow label="خروج از کشور" switchButtons={(status) => this.switchButtons(status, 7, 'transfer')} />
                                    </View> : null

                            }

                            <View style={styles.itemContainer}>
                                <TouchableOpacity onPress={() => Actions.results()} style={styles.advertise}>
                                    <Text style={styles.buttonTitle}>مقایسه کن!</Text>
                                </TouchableOpacity>
                            </View>
                            {
                                this.state.showPicker ?
                                    <View style={{
                                        position: 'absolute',
                                        bottom: '50%',
                                        zIndex: 9999,
                                        backgroundColor: 'white'
                                    }}>
                                        <PersianCalendarPicker
                                            onDateChange={(date) => this.onDateChange(date)}
                                        />
                                    </View>
                                    : null
                            }
                        </View>
                    </ScrollView>
                    <View style={styles.reminder}>
                        <Icon name="bell-o" size={25} color="white" />
                        <Text style={styles.reminderText}>یادآور بیمه</Text>
                    </View>
                    <FooterMenu active="home" />
                </View>
            );
    }
}
export default VacleInfo;

