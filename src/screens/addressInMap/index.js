import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput, ImageBackground,Animated,  Dimensions} from 'react-native';
import styles from './styles'
import tag from '../../assets/tag.png'
import {Actions} from 'react-native-router-flux'
import FIcon from 'react-native-vector-icons/dist/Feather';
import OIcon from 'react-native-vector-icons/dist/Octicons';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bani.azarinpro.info/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import PageHeader from '../../components/pageHeader'
import FooterMenu from '../../components/footerMenu'
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import MapView, { Marker } from 'react-native-maps';
import marker from '../../assets/marker.png'

class AddressInMap extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            name: '',
            tel: '',
            reciver: '',
            time: '',
            markers: [],
            lat: null,
            lng: null
        };
        this.onBackPress = this.onBackPress.bind(this);
        this.handlePress = this.handlePress.bind(this);

    }
    getInitialState() {
        return {
            coordinate: new AnimatedRegion({
                latitude: 50,
                longitude: 60,
            }),
        };
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

    }
    handlePress(e) {
        this.setState({
            markers: [
                {
                    coordinate: e.nativeEvent.coordinate,
                }
            ],
            lat: e.nativeEvent.coordinate.latitude,
            lng: e.nativeEvent.coordinate.longitude
        })
    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        } else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.topHeader}>
                            <PageHeader carInfo={false} title="مشخصات تحویل گیرنده" />
                        </View>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            <MapView
                                style={styles.mapStyle}
                                initialRegion={{
                                    latitude: 35.715298,
                                    longitude: 51.404343,
                                    latitudeDelta: 0.0922,
                                    longitudeDelta: 0.0421,
                                }}
                                onPress={this.handlePress}
                            >
                                {this.state.markers.map((marker, index) => {
                                    return (
                                        <Marker key={index} {...marker} >
                                            <Image source={marker} style={{width: 50, height: 50, resizeMode: 'contain'}} />
                                        </Marker>
                                    )
                                })}
                            </MapView>
                        </View>
                    </ScrollView>
                    <TouchableOpacity onPress={()=> null} style={styles.reminder}>
                        <Text style={styles.reminderText}>ثبت نشانی</Text>
                    </TouchableOpacity>
                    <FooterMenu active="home" />
                </View>
            );
    }
}
export default AddressInMap;
