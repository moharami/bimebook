

import { StyleSheet, Dimensions} from 'react-native';
// import env from '../../colors/env';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(246, 246, 246)',
        // backgroundColor: 'red'
    },
    header: {
        flex: 1,
        height: '22%',
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9990
    },
    topHeader: {
        backgroundColor: 'rgb(20, 85, 151)',
        width: '100%',
        height: '45%'
    },

    bodyContainer: {
        width: '100%',
        paddingBottom: 180,
        paddingTop: 10,
        backgroundColor: 'rgb(248, 248, 248)',
        alignItems: 'center',
        justifyContent: 'center',
    },
    scroll: {
        paddingTop: 45,
        backgroundColor: 'rgb(249, 249, 249)'
    },
    reminder: {
        position: 'absolute',
        right: '30%',
        bottom: 85,
        zIndex: 40,
        alignItems: 'center',
        justifyContent: 'center',

        width: '40%',
        borderRadius: 30,
        backgroundColor: '#8dc63f',
        padding: 11,
        marginTop: 35
    },
    reminderText: {
        fontSize: 13,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    headerContainer: {
        alignItems: 'flex-end',
        justifyContent: 'center',
        padding: 15,
        borderBottomWidth: 1,
        borderBottomColor: 'rgb(237, 237, 237)',
        backgroundColor: 'white'
    },
    headerText: {
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black',
        alignSelf: 'flex-end'
    },
    pickerContainer: {
        width: '90%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    advertise: {
        width: '40%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#8dc63f',
        padding: 8,
        marginTop: 35
    },
    buttonTitle: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    insuranceContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 20
    },
    mapText: {
        fontSize: 14,
        color: 'rgb(11, 81, 144)',
        fontFamily: 'IRANSansMobile(FaNum)',
        textDecorationLine: 'underline'
    },
    mapStyle: {
        width: '100%',
        height: Dimensions.get('screen').height
    }
});

