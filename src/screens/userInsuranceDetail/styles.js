

import { StyleSheet, Dimensions} from 'react-native';
// import env from '../../colors/env';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(246, 246, 246)',
        // backgroundColor: 'red'
    },
    header: {
        flex: 1,
        height: '22%',
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9990
    },
    topHeader: {
        backgroundColor: 'rgb(20, 85, 151)',
        width: '100%',
        height: '45%'
    },
    bodyContainer: {
        width: '100%',
        paddingBottom: 180,
        paddingTop: 20,
        padding: 10,
        backgroundColor: 'rgb(248, 248, 248)',
        alignItems: 'center',
        justifyContent: 'center',

    },
    insuranceInfoContainer: {
        width: '100%',
        padding: 10,
        backgroundColor: 'rgb(168, 170, 173)',
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        position: 'relative',
        zIndex: 1
    },
    notif: {
        position: 'absolute',
        zIndex: 300,
        top: 5,
        right: 5,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#fdb913',
        padding: 5,
    },
    insurancerContainer: {
        width: '100%',
        padding: 6,
        backgroundColor: 'white',
        borderRadius: 10
    },
    insuranceRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    imageContainer: {
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        padding: 10
    },
    rightContainer: {
        // flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 3,
        paddingLeft: 3,
        height: 27,
        // borderRadius: 20,
        backgroundColor: 'rgb(11, 81, 144)',
        // overflow: 'hidden'
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderTopLeftRadius: 10,
        marginRight: 5
        // overflow: 'hidden',

    },
    label: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'white'
    },
    title: {
        fontFamily: 'IRANSansMobile(FaNum)_Bold',
        fontSize: 18,
        color: 'white'
    },
    contentText: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'white',
        paddingRight: 10
    },
    labelContainer: {
        flexDirection: 'row',
        paddingRight: 10

    },
    shaped: {
        borderRadius: 10,
        // borderColor: 'rgb(142, 199, 61)',
        // borderWidth: 1,
        // width: '80%'
        // marginLeft: 17
    },
    top: {
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        paddingBottom: 2,
        paddingRight: 14,
        paddingLeft: 14,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgb(11, 81, 144)'
    },
    value: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14,
        color: 'black'
    },
    bottom: {
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent: 'center',
        backgroundColor: 'white'
    },
    left: {
        alignItems: 'flex-end',
        justifyContent: 'center',
        borderWidth: 2,
        borderColor: 'rgb(11, 81, 144)',
        borderBottomLeftRadius: 10,
        borderRightWidth: 0,
        padding: 2,
        paddingRight: 10,
        paddingLeft: 10

    },
    right: {
        alignItems: 'flex-end',
        justifyContent: 'center',
        borderWidth: 2,
        borderColor: 'rgb(11, 81, 144)',
        borderBottomRightRadius: 10,
        padding: 2,
        paddingRight: 10,
        paddingLeft: 10
    },
    startTxt: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 10,
        color: 'rgb(143, 235, 141)'
    },
    endTxt: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 10,
        color: 'red'
    },
    TxtValue: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 10,
        color: 'black'
    },
    topTxt: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'white',
    },
    bottomTxt: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'rgb(142, 199, 61)',
    },
    bodyLabel: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'black',
        alignSelf: 'flex-end',
        paddingTop: 15,
        paddingBottom: 5,
        paddingRight: 10
    },
    scroll: {
        paddingTop: 45,
        backgroundColor: 'rgb(249, 249, 249)'
    },
    reminder: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '40%',
        borderRadius: 30,
        backgroundColor: '#8dc63f',
        padding: 11,
        marginTop: 35
    },
    reminderText: {
        fontSize: 13,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    headerContainer: {
        alignItems: 'flex-end',
        justifyContent: 'center',
        padding: 15,
        borderBottomWidth: 1,
        borderBottomColor: 'rgb(237, 237, 237)',
        backgroundColor: 'white'
    },
    headerText: {
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black',
        alignSelf: 'flex-end'
    },
    pickerContainer: {
        width: '90%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    advertise: {
        width: '40%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#8dc63f',
        padding: 8,
        marginTop: 35
    },
    buttonTitle: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    insuranceContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 20
    },
    mapText: {
        fontSize: 14,
        color: 'rgb(11, 81, 144)',
        fontFamily: 'IRANSansMobile(FaNum)',
        textDecorationLine: 'underline'
    },
    mapStyle: {
        width: '100%',
        height: Dimensions.get('screen').height
    }
});

