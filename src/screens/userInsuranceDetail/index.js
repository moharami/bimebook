


import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput, ImageBackground,Animated,  Dimensions} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://bani.azarinpro.info/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import PageHeader from '../../components/pageHeader'
import FooterMenu from '../../components/footerMenu'
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import asia from '../../assets/asia.png'
import InfoItem from "../../components/infoItem/index";

class UserInsuranceDetail extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        } else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.topHeader}>
                            <PageHeader carInfo={false} title="جزئیات بیمه نامه" />
                        </View>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            <Text style={styles.bodyLabel}>مشخصات بیمه نامه</Text>
                            <View style={styles.insuranceInfoContainer}>
                                <View style={styles.notif}>
                                    <Icon name="bell-o" size={18} color="white" />
                                </View>
                                <View style={styles.shaped}>
                                    <View style={styles.top}>
                                        <Text style={styles.label}>بیمه شخص ثالث</Text>
                                    </View>
                                    <View style={styles.bottom}>
                                        <View style={styles.left}>
                                            <Text style={styles.endTxt}>تاریخ پایان</Text>
                                            <Text style={styles.TxtValue}>16:53-97/6/7</Text>
                                        </View>
                                        <View style={styles.right}>
                                            <Text style={styles.startTxt}>تاریخ شروع</Text>
                                            <Text style={styles.TxtValue}>16:53-97/6/7</Text>
                                        </View>
                                        {/*<EIcon name="close-o" size={18} color="red" />*/}
                                    </View>
                                </View>
                                <Text style={styles.title}>بیمه آسیا</Text>
                                <View style={styles.imageContainer}>
                                    <Image source={asia} style={{width: 65, height: 65, resizeMode: 'contain'}} />
                                </View>
                            </View>
                            <Text style={styles.bodyLabel}>جزئیات بیمه نامه</Text>
                            <View style={styles.insurancerContainer}>
                                <View style={[styles.insuranceRow, {marginBottom: 6}]}>
                                    <InfoItem label="پلاک" value="" />
                                    <InfoItem label="خودرو" value="" />
                                </View>
                                <View style={styles.insuranceRow}>
                                    <InfoItem label="شماره بدنه" value="" />
                                    <InfoItem label="شماره شاسی" value="" />
                                </View>
                            </View>
                            <Text style={styles.bodyLabel}>بیمه گذار</Text>
                            <View style={styles.insurancerContainer}>
                                <View style={[styles.insuranceRow, {marginBottom: 6}]}>
                                    <InfoItem label="کدملی" value="48484887745" />
                                    <InfoItem label="نام تحویل گیرنده" value="علی عباسی" />
                                </View>
                                <View style={styles.insuranceRow}>
                                    <InfoItem label="نشانی:" value="" long />
                                </View>
                            </View>
                            <TouchableOpacity onPress={()=> Actions.home()} style={styles.reminder}>
                                <Text style={styles.reminderText}>تمدید</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                    <FooterMenu active="home" />
                </View>
            );
    }
}
export default UserInsuranceDetail;
