import React, {Component} from 'react';
import {Router, Stack, Scene} from 'react-native-router-flux'
import {store} from '../config/store';
import {Provider, connect} from 'react-redux';
import Login from '../screens/login'
import Home from '../screens/home'
import Confirmation from '../screens/confirmation'
import VacleInfo from '../screens/vacleInfo'
import OtherVacleInfo from '../screens/otherVacleInfo'
import Results from '../screens/results'
import CarInfo from '../screens/carInfo'
import UploadDocs from '../screens/uploadDocs'
import CustomerInfo from '../screens/customerInfo'
import AddressInMap from '../screens/addressInMap'
import InsuranceExport from '../screens/insuranceExport'
import InstalmentExport from '../screens/instalmentExport'
import PaymentSuccess from '../screens/paymentSuccess'
import PaymentFailed from '../screens/paymentFailed'
import BuyMassage from '../screens/buyMassage'
import DeliveryMassage from '../screens/deliveryMassage'
import Reminder from '../screens/reminder'
import ReminderDetail from '../screens/reminderDetail'
import AddReminder from '../screens/addReminder'
import UserInsurances from '../screens/userInsurances'
import UserInsuranceDetail from '../screens/userInsuranceDetail'
import ExtendedInsurance from '../screens/extendedInsurance'
import Profile from '../screens/profile'
import AddAddress from '../screens/addAddress'
import Companies from '../screens/companies'
import CompanyItemDetail from '../screens/companyItemDetail'
import Courier from '../screens/courier'
import Transactions from '../screens/transactions'

class MainDrawerNavigator extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const RouterWidthRedux = connect()(Router);

        return (
            <Provider store={store}>
                <RouterWidthRedux>
                    <Scene>
                        <Stack key="container" hideNavBar
                        >
                            <Scene key="login" component={Login} title="Login" hideNavBar/>
                            <Scene key="confirmation" component={Confirmation} title="Confirmation" hideNavBar/>
                            <Scene key="home"  component={Home} title="Home" hideNavBar/>
                            <Scene key="vacleInfo" component={VacleInfo} title="VacleInfo" hideNavBar/>
                            <Scene key="otherVacleInfo" component={OtherVacleInfo} title="OtherVacleInfo" hideNavBar/>
                            <Scene key="results" component={Results} title="Results" hideNavBar/>
                            <Scene key="carInfo" component={CarInfo} title="CarInfo" hideNavBar/>
                            <Scene key="uploadDocs" component={UploadDocs} title="UploadDocs" hideNavBar/>
                            <Scene key="customerInfo" component={CustomerInfo} title="CustomerInfo" hideNavBar/>
                            <Scene key="addressInMap"  component={AddressInMap} title="AddressInMap" hideNavBar/>
                            <Scene key="insuranceExport" component={InsuranceExport} title="InsuranceExport" hideNavBar/>
                            <Scene key="instalmentExport" component={InstalmentExport} title="InstalmentExport" hideNavBar/>
                            <Scene key="paymentSuccess" component={PaymentSuccess} title="PaymentSuccess" hideNavBar/>
                            <Scene key="PaymentFailed" component={PaymentFailed} title="PaymentFailed" hideNavBar/>
                            <Scene key="buyMassage" component={BuyMassage} title="BuyMassage" hideNavBar/>
                            <Scene key="deliveryMassage" component={DeliveryMassage} title="DeliveryMassage" hideNavBar/>
                            <Scene key="reminder"  component={Reminder} title="Reminder" hideNavBar/>
                            <Scene key="reminderDetail" component={ReminderDetail} title="ReminderDetail" hideNavBar/>
                            <Scene key="addReminder" component={AddReminder} title="AddReminder" hideNavBar/>
                            <Scene key="userInsurances" component={UserInsurances} title="UserInsurances" hideNavBar/>
                            <Scene key="userInsuranceDetail" component={UserInsuranceDetail} title="UserInsuranceDetail" hideNavBar/>
                            <Scene key="extendedInsurance" component={ExtendedInsurance} title="ExtendedInsurance" hideNavBar/>
                            <Scene key="profile" component={Profile} title="Profile" hideNavBar/>
                            <Scene key="addAddress" component={AddAddress} title="AddAddress" hideNavBar/>
                            <Scene key="companies" component={Companies} title="Companies" hideNavBar/>
                            <Scene key="companyItemDetail" component={CompanyItemDetail} title="CompanyItemDetail" hideNavBar/>
                            <Scene key="courier" component={Courier} title="Courier" hideNavBar/>
                            <Scene key="transactions" initial component={Transactions} title="Transactions" hideNavBar/>
                        </Stack>
                    </Scene>
                </RouterWidthRedux>
            </Provider>
        );
    }
}
export default MainDrawerNavigator;



