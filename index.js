import React from 'react';
import { AppRegistry } from 'react-native';
import App from './src/app';
import { YellowBox } from 'react-native';
import {name as appName} from './app.json';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

const Application = () => {
    return ( <App/>
    );
};
AppRegistry.registerComponent(appName, () => App);
